package ro.h23.manager.core.flight;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
public class Flight {

	private int flight_id;
	private Plane plane;
	private String source_flight;
	private String destination_flight;
	private String source_aeroport;
	private String destination_aeropot;
	private String leave_date;
	private String leave_hour;
	private String arrive_date;
	private String arrive_hour;
	private int available_seats;
	private double price_seat;
	public int getFlight_id() {
		return flight_id;
	}
	public void setFlight_id(int flight_id) {
		this.flight_id = flight_id;
	}
	
	public Plane getPlane() {
		return plane;
	}
	public void setPlane(Plane plane) {
		this.plane = plane;
	}
	public String getSource_flight() {
		return source_flight;
	}
	public void setSource_flight(String source_flight) {
		this.source_flight = source_flight;
	}
	public String getDestination_flight() {
		return destination_flight;
	}
	public void setDestination_flight(String destination_flight) {
		this.destination_flight = destination_flight;
	}
	public String getSource_aeroport() {
		return source_aeroport;
	}
	public void setSource_aeroport(String source_aeroport) {
		this.source_aeroport = source_aeroport;
	}
	public String getDestination_aeropot() {
		return destination_aeropot;
	}
	public void setDestination_aeropot(String destination_aeropot) {
		this.destination_aeropot = destination_aeropot;
	}
	public String getLeave_date() {
		return leave_date;
	}
	public void setLeave_date(String leave_date) {
		this.leave_date = leave_date;
	}
	
	public String getLeave_hour() {
		return leave_hour;
	}
	public void setLeave_hour(String leave_hour) {
		this.leave_hour = leave_hour;
	}
	public String getArrive_hour() {
		return arrive_hour;
	}
	public void setArrive_hour(String arrive_hour) {
		this.arrive_hour = arrive_hour;
	}
	public String getArrive_date() {
		return arrive_date;
	}
	public void setArrive_date(String arrive_date) {
		this.arrive_date = arrive_date;
	}
	public int getAvailable_seats() {
		return available_seats;
	}
	public void setAvailable_seats(int available_seats) {
		this.available_seats = available_seats;
	}
	public double getPrice_seat() {
		return price_seat;
	}
	public void setPrice_seat(double price_seat) {
		this.price_seat = price_seat;
	}
	
}
