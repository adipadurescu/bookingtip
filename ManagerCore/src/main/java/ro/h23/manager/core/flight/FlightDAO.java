package ro.h23.manager.core.flight;

import java.sql.Connection;
import java.util.Date;
import java.util.List;




public interface FlightDAO {
	public Connection getConnection();
	public List<Flight> getFlightsByUserPreferences(String source, String destination, String leave, String arrive);
	public int createReservationFlight(ReservationFlight reservationFlight) ;
	public String getUserName(int id);
	public void updateSeatsAvailable(int id_flight,int seats_reservation);
	public int getSeatsAvailable(int id_flight);
	public Flight getInfoFlight(int id_flight);
}
