package ro.h23.manager.core.flight;

public class Plane {
	private int plane_id;
	private String company;
	private int capacity;
	private String logo;
	public int getPlane_id() {
		return plane_id;
	}
	public void setPlane_id(int plane_id) {
		this.plane_id = plane_id;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public Plane(int plane_id, String company, int capacity, String logo) {
		super();
		this.plane_id = plane_id;
		this.company = company;
		this.capacity = capacity;
		this.logo = logo;
	}
	public Plane() {
		
	}
	
}
