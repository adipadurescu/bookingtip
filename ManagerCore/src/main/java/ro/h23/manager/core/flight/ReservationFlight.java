package ro.h23.manager.core.flight;

public class ReservationFlight {
	private int reservation_id;
	private int user_id;
	private int flight_id;
	private double price_reservation;
	private int number_seats;
	private String leave_date;
	private String leave_hour;
	private String arrive_date;
	private String arrive_hour;
	public String getLeave_date() {
		return leave_date;
	}
	public void setLeave_date(String leave_date) {
		this.leave_date = leave_date;
	}
	public String getLeave_hour() {
		return leave_hour;
	}
	public void setLeave_hour(String leave_hour) {
		this.leave_hour = leave_hour;
	}
	public String getArrive_date() {
		return arrive_date;
	}
	public void setArrive_date(String arrive_date) {
		this.arrive_date = arrive_date;
	}
	public String getArrive_hour() {
		return arrive_hour;
	}
	public void setArrive_hour(String arrive_hour) {
		this.arrive_hour = arrive_hour;
	}
	public int getReservation_id() {
		return reservation_id;
	}
	public void setReservation_id(int reservation_id) {
		this.reservation_id = reservation_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getFlight_id() {
		return flight_id;
	}
	public void setFlight_id(int flight_id) {
		this.flight_id = flight_id;
	}
	public double getPrice_reservation() {
		return price_reservation;
	}
	public void setPrice_reservation(double price_reservation) {
		this.price_reservation = price_reservation;
	}
	public int getNumber_seats() {
		return number_seats;
	}
	public void setNumber_seats(int number_seats) {
		this.number_seats = number_seats;
	}
	
}
