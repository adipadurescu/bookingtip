package ro.h23.manager.core.hotel;

public class CheckAvailableRoom {
	private String checkIn;
	private String checkOut;
	private HotelRoom hotelRoom;
	

	public String getCheckIn() {
		return checkIn;
	}
	public void setCheckIn(String checkIn) {
		this.checkIn = checkIn;
	}
	public String getCheckOut() {
		return checkOut;
	}
	public void setCheckOut(String checkOut) {
		this.checkOut = checkOut;
	}
	public HotelRoom getHotelRoom() {
		return hotelRoom;
	}
	public void setHotelRoom(HotelRoom hotelRoom) {
		this.hotelRoom = hotelRoom;
	}
	
	
}
