package ro.h23.manager.core.hotel;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Hotel {
	
	private int hotel_id;
	private String name;
	private String location;
	private String phone_number;
	private String description;
	private int star_rating;
	private String category;
	private String picture; //path 
	
	private int minPriceChoseByUser;
	private int maxPriceChoseByUser;
	
	public int getMinPriceChoseByUser() {
		return minPriceChoseByUser;
	}
	public void setMinPriceChoseByUser(int minPriceChoseByUser) {
		this.minPriceChoseByUser = minPriceChoseByUser;
	}
	
	public int getMaxPriceChoseByUser() {
		return maxPriceChoseByUser;
	}
	public void setMaxPriceChoseByUser(int maxPriceChoseByUser) {
		this.maxPriceChoseByUser = maxPriceChoseByUser;
	}
	public int getHotel_id() {
		return hotel_id;
	}
	public void setHotel_id(int hotel_id) {
		this.hotel_id = hotel_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getPhone_number() {
		return phone_number;
	}
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getStar_rating() {
		return star_rating;
	}
	public void setStar_rating(int star_rating) {
		this.star_rating = star_rating;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	
	
	
}
