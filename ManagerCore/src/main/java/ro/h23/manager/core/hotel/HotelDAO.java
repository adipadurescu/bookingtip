package ro.h23.manager.core.hotel;

import java.sql.Connection;
import java.util.List;

public interface HotelDAO {
	public Connection getConnection();
	public  List<Hotel> getAllHotels();
	public List<Hotel> getHotelsByUserPreferences(String location);
}
