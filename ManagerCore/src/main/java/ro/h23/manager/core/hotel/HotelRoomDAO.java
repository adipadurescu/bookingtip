package ro.h23.manager.core.hotel;

import java.sql.Connection;
import java.util.List;

public interface HotelRoomDAO {
	public Connection getConnection();
	public List<HotelRoom> getHotelRoomsByHotelId(int id);
}
