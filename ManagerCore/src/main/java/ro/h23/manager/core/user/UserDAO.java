package ro.h23.manager.core.user;

import java.sql.Connection;
import java.util.List;

public interface UserDAO {
	
	public Connection getConnection() ;
	public int registerUser(User user);	
	public List<User> getUsers();
	public int updateUser(User user);
	public String checkLogin(User user);
	
	public int insertDataInUserLogin(User user);
	public User getUserDataByEmail(String email) ;

}
