package ro.h23.manager.client.flights;

import java.io.IOException;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import ro.h23.manager.core.flight.Flight;
import ro.h23.manager.core.flight.Plane;
import ro.h23.manager.core.hotel.Hotel;

@WebServlet("/PrintFlightsByCompanyServlet")
public class PrintFlightsByCompanyServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

    public PrintFlightsByCompanyServlet() {
        super();
    }

    private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8081/ManagerWebServices").build();
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget service = client.target(getBaseURI());
		HttpSession session=request.getSession(true);
		
		String category = request.getParameter("category");
	/*	String source = request.getParameter("source");
		String destination = request.getParameter("destination");
		String leave_date =  request.getParameter("checkIn");
		String arrive_date = request.getParameter("checkOut");
*/
		String source =(String) session.getAttribute("source");
		String destination = (String) session.getAttribute("destination");
		String leave_date =  (String) session.getAttribute("checkIn");
		String arrive_date = (String) session.getAttribute("checkOut");
		System.out.println(source);
		System.out.println(destination);
		System.out.println(leave_date);
		System.out.println(arrive_date);
		
		Flight flight = new Flight();
		flight.setSource_flight(source);
		flight.setDestination_flight(destination);
		flight.setLeave_date(leave_date);
		flight.setArrive_date(arrive_date);
		Plane p = new Plane();
		p.setCompany(category);
		flight.setPlane(p);
		
		if (!category.equals("")) {

			String res = service.path("rest").path("printFlightsByCategory").request(MediaType.TEXT_HTML)
					.post(Entity.entity(flight, MediaType.APPLICATION_JSON), String.class);
			
			if (!res.equals("")) {
				
				request.setAttribute("printAllFlights", res);
				request.getRequestDispatcher("flights.jsp").forward(request, response);

				
			} else {
				request.setAttribute("noHotelsToShow", "Sorry! Couldn't find any flights.");
				request.getRequestDispatcher("flights.jsp").forward(request, response);

			}	
		}
	}

}
