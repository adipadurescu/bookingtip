package ro.h23.manager.client.flights;

import java.io.IOException;
import java.net.URI;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import org.glassfish.jersey.client.ClientConfig;
import ro.h23.manager.core.flight.Flight;

@WebServlet("/PrintFlightsServlet")
public class PrintFlightsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public PrintFlightsServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8081/ManagerWebServices").build();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget service = client.target(getBaseURI());

		String source = request.getParameter("source");
		String destination = request.getParameter("destination");
		String number_seats=request.getParameter("people");
		String leave_date = request.getParameter("checkIn");
		String arrive_date = request.getParameter("checkOut");

	
		if (!source.equals("") && !destination.equals("") && !leave_date.equals("") && !arrive_date.equals("")) {

			String[] parts = leave_date.split("/");
			if (Double.parseDouble(parts[0]) < 10) {
				parts[0] = "0" + parts[0];
			}
			if (Double.parseDouble(parts[1]) < 10) {
				parts[1] = "0" + parts[1];
			}
			String concat_leave_date = parts[2] + "-" + parts[0] + "-" + parts[1];

			String[] parts1 = arrive_date.split("/");
			if (Double.parseDouble(parts1[0]) < 10) {
				parts1[0] = "0" + parts1[0];
			}
			if (Double.parseDouble(parts1[1]) < 10) {
				parts1[1] = "0" + parts1[1];
			}
			String concat_arrive_date = parts1[2] + "-" + parts1[0] + "-" + parts1[1];
			System.out.println(concat_arrive_date);
			System.out.println(concat_leave_date);
			Flight flight = new Flight();
			flight.setSource_flight(source);
			flight.setDestination_flight(destination);
			flight.setLeave_date(concat_leave_date);
			flight.setArrive_date(concat_arrive_date);

			String res = service.path("rest").path("printAllFlights").request(MediaType.TEXT_HTML)
					.post(Entity.entity(flight, MediaType.APPLICATION_JSON), String.class);

			HttpSession session = request.getSession();

			if (!res.equals("")) {
				if (!leave_date.equals("") && !arrive_date.equals("")) {

					session.setAttribute("checkIn", concat_leave_date);
					session.setAttribute("checkOut", concat_arrive_date);
				}
				request.setAttribute("source", source);
				request.setAttribute("destination", destination);
				session.setAttribute("source", source);
				session.setAttribute("destination", destination);
				request.setAttribute("printAllFlights", res);
				request.getRequestDispatcher("flights.jsp").forward(request, response);
				session.setAttribute("number_seats", number_seats);

			} else {
				request.setAttribute("noFlightsToShow", "Sorry! Couldn't find any flights.");
				request.getRequestDispatcher("flights.jsp").forward(request, response);

			}

		} else {
			request.setAttribute("noFlightsToShow", "Sorry! Couldn't find any flights because you need to complete all fields!");
			request.getRequestDispatcher("flights.jsp").forward(request, response);

		}

	}

}
