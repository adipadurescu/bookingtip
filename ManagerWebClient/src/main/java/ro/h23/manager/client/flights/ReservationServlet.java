package ro.h23.manager.client.flights;

import java.io.IOException;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import org.glassfish.jersey.client.ClientConfig;
import ro.h23.manager.core.flight.ReservationFlight;
import ro.h23.manager.core.user.User;



@WebServlet("/ReservationServlet")
public class ReservationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ReservationServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8081/ManagerWebServices").build();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget service = client.target(getBaseURI());

		HttpSession session = request.getSession(true);
		String source = (String) session.getAttribute("source");
		String destination = (String) session.getAttribute("destination");
		String leave_date = (String) session.getAttribute("checkIn");
		String arrive_date = (String) session.getAttribute("checkOut");

		int flight_id = Integer.parseInt(request.getParameter("flight_id"));
		double price_reservation = Double.parseDouble(request.getParameter("price"));
		int available_seats = Integer.parseInt(request.getParameter("seats"));
		String number_seats_reservation = (String) session.getAttribute("number_seats");
		String leave_hour=request.getParameter("leave");
		String arrive_hour=request.getParameter("arrive");

		if (Integer.parseInt(number_seats_reservation) < available_seats) {
			User user = new User();
			user.setEmail((String) session.getAttribute("token"));

			User userResponse = service.path("rest").path("getUserIdKnowingEmail").request(MediaType.APPLICATION_JSON)
					.post(Entity.entity(user, MediaType.APPLICATION_JSON), User.class);
			price_reservation=Integer.parseInt(number_seats_reservation)*price_reservation;
			ReservationFlight reservationFlight = new ReservationFlight();
			reservationFlight.setFlight_id(flight_id);
			reservationFlight.setPrice_reservation(price_reservation);
			reservationFlight.setUser_id(userResponse.getId());
			reservationFlight.setLeave_date(leave_date);
			reservationFlight.setArrive_date(arrive_date);
			reservationFlight.setNumber_seats(Integer.parseInt(number_seats_reservation));
			reservationFlight.setArrive_hour(arrive_hour);
			reservationFlight.setLeave_hour(leave_hour);
			String reservationStatus = service.path("rest").path("createReservationFlight").request(MediaType.TEXT_HTML)
					.post(Entity.entity(reservationFlight, MediaType.APPLICATION_JSON), String.class);

			request.setAttribute("reservationStatus", reservationStatus);
			request.getRequestDispatcher("flight_reservation.jsp").forward(request, response);
		} else {
			request.setAttribute("reservationStatus",
					"<h3 style=\"color:#ff8080; text-align:center\">Unavailable left seats for the specified flight.</h3>");
			request.getRequestDispatcher("flight_reservation.jsp").forward(request, response);
		}

	}

}
