package ro.h23.manager.client.hotel;

import java.io.IOException;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import ro.h23.manager.core.hotel.HotelRoom;

/**
 * Servlet implementation class PrintHotelRoomsServlet
 */
@WebServlet("/PrintHotelRoomsServlet")
public class PrintHotelRoomsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public PrintHotelRoomsServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8081/ManagerWebServices").build();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget service = client.target(getBaseURI());

		String hotel_id = request.getParameter("hotel_id");
		HotelRoom hotelRoom = new HotelRoom();
		hotelRoom.setHotel_id(Integer.parseInt(hotel_id));

		String res = service.path("rest").path("printHotelRooms").request(MediaType.TEXT_HTML).post(Entity.entity(hotelRoom, MediaType.APPLICATION_JSON), String.class);
		
		if (!res.equals("")) {

			request.setAttribute("printHotelRooms", res);
			request.getRequestDispatcher("hotel_rooms.jsp").forward(request, response);
			
		} else {
			request.setAttribute("noHotelRoomsToShow", "Sorry! Couldn't find any hotel rooms.");
			request.getRequestDispatcher("hotel_rooms.jsp").forward(request, response);

		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
