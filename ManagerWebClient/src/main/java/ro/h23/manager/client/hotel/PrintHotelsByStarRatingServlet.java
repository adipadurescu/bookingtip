package ro.h23.manager.client.hotel;

import java.io.IOException;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import ro.h23.manager.core.hotel.Hotel;

@WebServlet("/PrintHotelsByStarRatingServlet")
public class PrintHotelsByStarRatingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public PrintHotelsByStarRatingServlet() {
        super();
    }

    private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8081/ManagerWebServices").build();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget service = client.target(getBaseURI());

		int star_rating = Integer.parseInt(request.getParameter("starRating"));
		String location = request.getParameter("location");
		if (!location.equals("")) {

			Hotel hotel = new Hotel();
			hotel.setLocation(location);
			hotel.setStar_rating(star_rating);
			String res = service.path("rest").path("printHotelsByStarRating").request(MediaType.TEXT_HTML)
					.post(Entity.entity(hotel, MediaType.APPLICATION_JSON), String.class);
			
			
			if (!res.equals("")) {
				
				request.setAttribute("printAllHotels", res);
				request.getRequestDispatcher("hotels.jsp").forward(request, response);

				
			} else {
				request.setAttribute("noHotelsToShow", "Sorry! Couldn't find any hotels.");
				request.getRequestDispatcher("hotels.jsp").forward(request, response);

			}

		}else {
			Hotel hotel = new Hotel();
			hotel.setStar_rating(star_rating);
			String res = service.path("rest").path("printHotelsByStarRating").request(MediaType.TEXT_HTML)
					.post(Entity.entity(hotel, MediaType.APPLICATION_JSON), String.class);
			
			if (!res.equals("")) {
				
				request.setAttribute("printAllHotels", res);
				request.getRequestDispatcher("hotels.jsp").forward(request, response);

				
			} else {
				request.setAttribute("noHotelsToShow", "Sorry! Couldn't find any hotels.");
				request.getRequestDispatcher("hotels.jsp").forward(request, response);

			}
		}
	}

}
