package ro.h23.manager.client.hotel;

import java.io.IOException;
//import java.io.PrintWriter;
import java.net.URI;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import ro.h23.manager.core.hotel.Hotel;

/**
 * Servlet implementation class PrintHotelsServlet
 */
@WebServlet("/PrintHotelsServlet")
public class PrintHotelsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public PrintHotelsServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8081/ManagerWebServices").build();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget service = client.target(getBaseURI());
		
		String res = service.path("rest").path("printAllHotels").request().accept(MediaType.TEXT_HTML)
				.get(String.class);

		request.setAttribute("printAllHotels", res);
		request.getRequestDispatcher("hotels.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget service = client.target(getBaseURI());

		String location = request.getParameter("location");
		String checkIn = request.getParameter("checkIn");
		String checkOut = request.getParameter("checkOut");
	
		if (!location.equals("")) {

			Hotel hotel = new Hotel();
			hotel.setLocation(location);

			String res = service.path("rest").path("printHotelsByUserPreferences").request(MediaType.TEXT_HTML)
					.post(Entity.entity(hotel, MediaType.APPLICATION_JSON), String.class);
			
			HttpSession session = request.getSession();
			
			if (!res.equals("")) {
				if (!checkIn.equals("") && !checkOut.equals("")) {
					
					session.setAttribute("checkIn", checkIn);
					session.setAttribute("checkOut", checkOut);
				}
				request.setAttribute("location",location);
				request.setAttribute("printAllHotels", res);
				request.getRequestDispatcher("hotels.jsp").forward(request, response);

				
			} else {
				request.setAttribute("noHotelsToShow", "Sorry! Couldn't find any hotels.");
				request.getRequestDispatcher("hotels.jsp").forward(request, response);

			}

		} else {
			String res = service.path("rest").path("printAllHotels").request().accept(MediaType.TEXT_HTML)
					.get(String.class);

			if (!checkIn.equals("") && !checkOut.equals("")) {
				HttpSession session = request.getSession();
				session.setAttribute("checkIn", checkIn);
				session.setAttribute("checkOut", checkOut);
			}

			request.setAttribute("printAllHotels", res);
			request.getRequestDispatcher("hotels.jsp").forward(request, response);
		}

	}

}
