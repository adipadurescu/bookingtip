package ro.h23.manager.client.hotel;

import java.io.IOException;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import ro.h23.manager.core.hotel.CheckAvailableRoom;
import ro.h23.manager.core.hotel.HotelRoom;
import ro.h23.manager.core.hotel.Reservation;
import ro.h23.manager.core.user.User;

/**
 * Servlet implementation class ReservationCompletedServlet
 */
@WebServlet("/ReservationCompletedServlet")
public class ReservationCompletedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ReservationCompletedServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    private static URI getBaseURI() {
 		return UriBuilder.fromUri("http://localhost:8081/ManagerWebServices").build();
 	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget service = client.target(getBaseURI());
		if(!(request.getParameter("hotel_id").equals("")|| request.getParameter("type_room").equals(""))) {
			HttpSession session=request.getSession();
			HotelRoom hotelRoom=new HotelRoom();
			String hotel_id=request.getParameter("hotel_id");
			hotelRoom.setHotel_id(Integer.parseInt(hotel_id));
			String type=request.getParameter("type_room");
			hotelRoom.setType(type);
			CheckAvailableRoom checkAvailableRoom= new CheckAvailableRoom();
			checkAvailableRoom.setHotelRoom(hotelRoom);
			checkAvailableRoom.setCheckIn((String)session.getAttribute("checkIn"));
			checkAvailableRoom.setCheckOut((String)session.getAttribute("checkOut"));
			
			//PrintWriter ot = response.getWriter();
			//ot.print(type);
			
			HotelRoom room = service.path("rest").path("returnAvailableRoom").request(MediaType.APPLICATION_JSON).post(Entity.entity(checkAvailableRoom, MediaType.APPLICATION_JSON),HotelRoom.class);
			
			if(room!=null) {
				
				User user=new User();
				user.setEmail((String)session.getAttribute("token"));
				
				User userResponse=service.path("rest").path("getUserIdKnowingEmail").request(MediaType.APPLICATION_JSON).post(Entity.entity(user, MediaType.APPLICATION_JSON),User.class);
				
				Reservation reservation = new Reservation();
				reservation.setCheck_in((String)session.getAttribute("checkIn"));
				reservation.setCheck_out((String)session.getAttribute("checkOut"));
				reservation.setHotel_id(Integer.parseInt(hotel_id));
				reservation.setRoom_number(room.getRoom_number());
				reservation.setSpecial_requests(request.getParameter("requests"));
				reservation.setUser_id(userResponse.getId());
				
				String reservationStatus = service.path("rest").path("createReservation").request(MediaType.TEXT_HTML).post(Entity.entity(reservation, MediaType.APPLICATION_JSON),String.class);
				
				request.setAttribute("reservationStatus", reservationStatus);
				request.getRequestDispatcher("reservations.jsp").forward(request, response);
			}else {
				request.setAttribute("reservationStatus", "<h3 style=\"color:#ff8080; text-align:center\">Unavailable rooms for the specified period.</h3>");
				request.getRequestDispatcher("reservations.jsp").forward(request, response);
			}
		}else {
			response.sendRedirect("PrintHotelsServlet");
		}
		
	}

}
