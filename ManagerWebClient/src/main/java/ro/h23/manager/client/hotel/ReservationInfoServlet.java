package ro.h23.manager.client.hotel;

import java.io.IOException;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import ro.h23.manager.core.hotel.CheckAvailableRoom;
import ro.h23.manager.core.hotel.HotelRoom;
import ro.h23.manager.core.user.User;

/**
 * Servlet implementation class MakeReservationServlet
 */
@WebServlet("/ReservationInfoServlet")
public class ReservationInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ReservationInfoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8081/ManagerWebServices").build();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget service = client.target(getBaseURI());
		
		HttpSession session=request.getSession();
		if(session.getAttribute("token")==null) {
			response.sendRedirect("login.jsp");
		}else {
			if(session.getAttribute("checkIn")!=null && session.getAttribute("checkOut")!=null) {
				
				//for printing hotel room data on reservation form
				HotelRoom hotelRoom=new HotelRoom();
				String hotel_id=request.getParameter("hotel_id");
				hotelRoom.setHotel_id(Integer.parseInt(hotel_id));
				String type=request.getParameter("type");
				hotelRoom.setType(type);
				
				//PrintWriter ot = response.getWriter();
				//ot.print(type);
				CheckAvailableRoom checkAvailableRoom= new CheckAvailableRoom();
				checkAvailableRoom.setHotelRoom(hotelRoom);
				checkAvailableRoom.setCheckIn((String)session.getAttribute("checkIn"));
				checkAvailableRoom.setCheckOut((String)session.getAttribute("checkOut"));
				
				String check = service.path("rest").path("checkAvailableRooms").request(MediaType.TEXT_HTML).post(Entity.entity(checkAvailableRoom, MediaType.APPLICATION_JSON),String.class);
				request.setAttribute("reservationData", check);					
				
				//need this information again when i press the button and return an available room instead html code
				request.setAttribute("hotel_id",hotel_id);
				request.setAttribute("type_room",type);
				
				//for printing user data on reservetion form
				String email=(String)session.getAttribute("token");
				User user = new User();
				user.setEmail(email);
				
				String res = service.path("rest").path("printUserDataByEmail").request(MediaType.TEXT_HTML).post(Entity.entity(user, MediaType.APPLICATION_JSON), String.class);
				request.setAttribute("userPersonalData", res);
				request.getRequestDispatcher("reservations.jsp").forward(request, response);
				
				
				
				
			}else {
				String checkInMsg="Please introduce check in date and check out date to complete the reservation.";
				//String hotel_id=request.getParameter("hotel_id");
				request.setAttribute("checkInMsg", checkInMsg);
				request.getRequestDispatcher("hotel_rooms.jsp").forward(request, response);
			}
			
		}
	}

}
