package ro.h23.manager.client.user;

import java.io.IOException;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import ro.h23.manager.core.user.User;

/**
 * Servlet implementation class ChangePasswordServlet
 */
@WebServlet("/ChangePasswordServlet")
public class ChangePasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public ChangePasswordServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    private static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost:8081/ManagerWebServices").build();
 }
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ClientConfig config = new ClientConfig();
		Client client=ClientBuilder.newClient(config);
		WebTarget service = client.target(getBaseURI()); 
		 
	    String password=request.getParameter("password");  
	    String repeatPassword=request.getParameter("repeatPassword");    
	    
	    String email;

	    if(request.getParameter("email") == null) {
	    	HttpSession session = request.getSession();
			email = (String) session.getAttribute("token");
	    }
	    else {
	    	email=request.getParameter("email");
	    }
	     
	    if (password.equals(repeatPassword)) {
			User user = new User();
			user.setEmail(email);
			user.setPassword(repeatPassword);
			Boolean res = service.path("rest").path("changePasswordByEmail").request(MediaType.TEXT_PLAIN)
					.post(Entity.entity(user, MediaType.APPLICATION_JSON), Boolean.class);
			if (res == true) {
				response.sendRedirect("login.jsp");
			} else {
				request.setAttribute("passwordMs", "There was a problem. Password did not change.");
				request.getRequestDispatcher("changePassword.jsp").forward(request, response);
			}
		} else {
			request.setAttribute("passwordMs", "Password do not match.");
			request.getRequestDispatcher("changePassword.jsp").forward(request, response);
		}

	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ClientConfig config = new ClientConfig();
		Client client=ClientBuilder.newClient(config);
		WebTarget service = client.target(getBaseURI()); 
		 
	    String password=request.getParameter("password");  
	    String repeatPassword=request.getParameter("repeatPassword");    
	    
	    String email;

	    
	    email=request.getParameter("email");
	   
	     
	    if (password.equals(repeatPassword)) {
			User user = new User();
			user.setEmail(email);
			user.setPassword(repeatPassword);
			Boolean res = service.path("rest").path("changePasswordByEmail").request(MediaType.TEXT_PLAIN)
					.post(Entity.entity(user, MediaType.APPLICATION_JSON), Boolean.class);
			if (res == true) {
				response.sendRedirect("login.jsp");
			} else {
				request.setAttribute("passwordMs", "There was a problem. Password did not change.");
				request.getRequestDispatcher("changePassword.jsp").forward(request, response);
			}
		} else {
			request.setAttribute("passwordMs", "Password do not match.");
			request.getRequestDispatcher("changePassword.jsp").forward(request, response);
		}

	}

}
