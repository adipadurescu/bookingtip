package ro.h23.manager.client.user;

import java.io.IOException;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import ro.h23.manager.core.user.User;

/**
 * Servlet implementation class ForgotPasswordServlet
 */
@WebServlet("/ForgotPasswordServlet")
public class ForgotPasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ForgotPasswordServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8081/ManagerWebServices").build();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget service = client.target(getBaseURI());

		String email = request.getParameter("email");

		User user = new User();
		user.setEmail(email);
		
		User res = service.path("rest").path("getUserIdKnowingEmail").request(MediaType.APPLICATION_JSON)
				.post(Entity.entity(user, MediaType.APPLICATION_JSON), User.class);

		if (res != null) {
			service.path("rest").path("sendForgotPasswordMail").request(MediaType.TEXT_PLAIN)
					.post(Entity.entity(user, MediaType.APPLICATION_JSON), Boolean.class);
			request.setAttribute("status", "The email was sent.");
			request.getRequestDispatcher("forgotPassword.jsp").forward(request, response);
		} else {
			request.setAttribute("status", "The email is not in the database.");
			request.getRequestDispatcher("forgotPassword.jsp").forward(request, response);
		}
	}

}
