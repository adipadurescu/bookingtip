package ro.h23.manager.client.user;

import java.io.IOException;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import ro.h23.manager.core.user.LoginValidator;
import ro.h23.manager.core.user.User;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public LoginServlet() {
		super();
	}

	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8081/ManagerWebServices").build();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget service = client.target(getBaseURI());
	
		String email = request.getParameter("email");
		String password = request.getParameter("password");

		User user = new User();
		user.setEmail(email);
		user.setPassword(password);

		LoginValidator res = service.path("rest").path("login").request(MediaType.APPLICATION_JSON).post(Entity.entity(user, MediaType.APPLICATION_JSON), LoginValidator.class);
		if (res.getValidation().equals("success")) {
			HttpSession session=request.getSession();
			session.setAttribute("token", res.getEmail());
			response.sendRedirect("home.jsp");
		} else {
	    	 request.setAttribute("status", "Password or email incorrect. Please try again");
	    	 request.getRequestDispatcher("login.jsp").forward(request, response);

		}


	}

}
