package ro.h23.manager.client.user;

import java.io.IOException;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import ro.h23.manager.core.user.User;

/**
 * Servlet implementation class ProfileServlet
 */
@WebServlet("/ProfileServlet")
public class ProfileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
    public ProfileServlet() {
        super();
    }

    private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8081/ManagerWebServices").build();
	}
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget service = client.target(getBaseURI());
		
		HttpSession session=request.getSession();
		
		if(session.getAttribute("token")==null) {
			response.sendRedirect("login.jsp");
		}else {
			String email=(String)session.getAttribute("token");
			User user = new User();
			user.setEmail(email);
			
			String res = service.path("rest").path("printUserDataByEmail").request(MediaType.TEXT_HTML).post(Entity.entity(user, MediaType.APPLICATION_JSON), String.class);
			request.setAttribute("userPersonalData", res);
			
			String resR = service.path("rest").path("printUserReservation").request(MediaType.TEXT_HTML).post(Entity.entity(user, MediaType.APPLICATION_JSON), String.class);
			request.setAttribute("userReservation", resR);
			
			String restR = service.path("rest").path("printUserFlights").request(MediaType.TEXT_HTML).post(Entity.entity(user, MediaType.APPLICATION_JSON), String.class);
			request.setAttribute("userFlight", restR);
			
			request.getRequestDispatcher("profile.jsp").forward(request, response);
		}
	}

}
