package ro.h23.manager.client.user;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import ro.h23.manager.core.user.User;


@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public RegisterServlet() {
        super();
    }

	 private static URI getBaseURI() {
	        return UriBuilder.fromUri("http://localhost:8081/ManagerWebServices").build();
	 }
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ClientConfig config = new ClientConfig();
		Client client=ClientBuilder.newClient(config);
		WebTarget service = client.target(getBaseURI()); 
		
	    PrintWriter out=response.getWriter();  
	    String first_name=request.getParameter("first_name");  
	    String last_name=request.getParameter("last_name");   
	    String email=request.getParameter("email");  
	    String phone_number=request.getParameter("phone_number"); 
	    String password=request.getParameter("password"); 
	    
	    User user=new User();
	    user.setFirst_name(first_name);
	    user.setLast_name(last_name);
        user.setEmail(email);
        user.setPhone_number(phone_number);
	    user.setPassword(password);
	     
	     Boolean res=service.path("rest").path("register").request(MediaType.TEXT_PLAIN).post(Entity.entity(user, MediaType.APPLICATION_JSON),Boolean.class);
	     out.println(res);
	     if(res==true) {
	    	 service.path("rest").path("sendRegistrationMail").request(MediaType.TEXT_PLAIN).post(Entity.entity(user, MediaType.APPLICATION_JSON),Boolean.class);
	    	 response.sendRedirect("login.jsp");
	     }else {
	    	 request.setAttribute("status", "Registration failed. Please try again.");
	    	 request.getRequestDispatcher("register.jsp").forward(request, response);
	     }
	}

}
