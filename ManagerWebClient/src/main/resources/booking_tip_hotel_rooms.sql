-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: booking_tip
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hotel_rooms`
--

DROP TABLE IF EXISTS `hotel_rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel_rooms` (
  `room_id` int(50) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(50) NOT NULL,
  `room_number` int(50) DEFAULT NULL,
  `price` int(50) DEFAULT NULL,
  `type` varchar(256) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `picture` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`room_id`),
  KEY `hotel_id_fk_idx` (`hotel_id`),
  CONSTRAINT `hotel_id_fk` FOREIGN KEY (`hotel_id`) REFERENCES `hotels` (`hotel_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotel_rooms`
--

LOCK TABLES `hotel_rooms` WRITE;
/*!40000 ALTER TABLE `hotel_rooms` DISABLE KEYS */;
INSERT INTO `hotel_rooms` VALUES (1,1,100,100,'Presidential Suite','Located on the 7th floor, this suite offers panoramic views over the Acropolis and the city of Athens. It has a spacious living room with a meeting table, a master bedroom and 2 bathrooms, one with a bathtub and one with spa bath. It includes air conditioning, 43\'\' LCD TV, direct dial phone, mini bar and espresso coffee maker. Free WiFi is available.','images_rooms/WGAPresidentialSuite.jpg');
INSERT INTO `hotel_rooms` VALUES (2,1,101,80,'Standard Double Room with City View','Fitted with pillow-top mattresses and Egyptian linens, this room offers city views. It includes air conditioning, 43\'\' LCD TV, direct dial phone, mini bar and espresso coffee maker. The modern bathroom is stocked with branded amenities, hairdryer, bathrobes and slippers. Free WiFi is available.','images_rooms/WGAStandargDoubleRoom.jpg');
INSERT INTO `hotel_rooms` VALUES (3,1,102,90,'Executive Suite Acropolis View','Offering views over the Acropolis, this executive suite has a king-size bed, pillow-top mattresses and Egyptian linen. It includes air conditioning, 43\'\' LCD TV, direct dial phone, mini bar and espresso coffee maker. The modern bathroom has a spa bath and is stocked with branded amenities, hairdryer, bathrobes and slippers. Free WiFi is available.','images_rooms/WGAExecutiveSuite.jpg');
INSERT INTO `hotel_rooms` VALUES (4,2,100,100,'Double Room','Rooms are located on the ground floor or second floor. They come with a bathtub.','images_rooms/PPBangkokDoubleRoom.jpg');
INSERT INTO `hotel_rooms` VALUES (5,2,101,120,'Luxury Suite','Elegantly designed, this spacious and luxurious suite features a separate living area and deluxe bathroom with river and garden views. ','images_rooms/PPBangkokLuxurySuite.jpg');
INSERT INTO `hotel_rooms` VALUES (6,2,102,120,'Luxury Suite','Elegantly designed, this spacious and luxurious suite features a separate living area and deluxe bathroom with river and garden views. ','images_rooms/PPBangkokLuxurySuite.jpg');
INSERT INTO `hotel_rooms` VALUES (7,2,103,120,'Luxury Suite','Elegantly designed, this spacious and luxurious suite features a separate living area and deluxe bathroom with river and garden views. ','images_rooms/PPBangkokLuxurySuite.jpg');
INSERT INTO `hotel_rooms` VALUES (8,3,100,50,'Double Room','Located on the 2nd floor, this room has a queen-sized bed and 2 single-sized futon beds on the loft. It offers air conditioning, a ceiling fan and shared bathroom facilities.','images_rooms/AtaalayaLipaDoubleRoom.jpg');
INSERT INTO `hotel_rooms` VALUES (9,4,100,90,'Deluxe Room','Located on a high floor, this air-conditioned room comes with 2 single beds, a flat-screen TV and personal safe. En suite bathroom has glass doors and a shower.','images_rooms/RoyalParkKyotoDeluxeRoom.jpg');
INSERT INTO `hotel_rooms` VALUES (10,5,100,120,'Double Room','Featuring views of the Eiffel Tower, this air-conditioned room is non-smoking and offers a Nespresso machine, a minibar and a seating area with a flat-screen TV. The bathroom is fitted with a hairdryer, free toiletries and a bath or shower. ','images_rooms/TerrassParis.jpg');
INSERT INTO `hotel_rooms` VALUES (11,6,100,100,'Three-Bedroom Apartment','With free Wi-Fi access, this apartment boasts wrought-iron beds and comes with a seating area with a sofa and satellite LCD TV. The kitchen is equipped with a dishwasher and a washing machine is available, as well.','images_rooms/CasaSerenaRoomTuscany.jpg');
INSERT INTO `hotel_rooms` VALUES (12,7,100,120,'Nile Deluxe Room','Each of Kempinski’s rooms offer free WiFi and high definition LCD TVs with satellite channels and a free minibar with soft drinks. For guests’ comfort, it offers a butler service and pillow concierge. ','images_rooms/KempinskiCairoDeluxeRoom.jpg');
INSERT INTO `hotel_rooms` VALUES (13,8,100,100,'Mini Suite Mountain View Room','This mini suite features a balcony with mountain views and includes a raised living area fitted with a sofa and a coffee table. Fully furnished with wooden designs, it also comes with mood-enhancing lighting and an en suite bathroom with a bathtub and shower.','images_rooms/RoyalCliffBaliMiniSuite.jpg');
INSERT INTO `hotel_rooms` VALUES (14,8,101,110,'One-Bedroom Theme Suite','This suite features a balcony, cable TV and sofa.','images_rooms/RoyalCliffBaliOneBedroom.jpg');
/*!40000 ALTER TABLE `hotel_rooms` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-18 17:03:49
