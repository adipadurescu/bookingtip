-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: booking_tip
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hotels`
--

DROP TABLE IF EXISTS `hotels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotels` (
  `hotel_id` int(50) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `location` varchar(256) DEFAULT NULL,
  `phone_number` varchar(256) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `star_rating` int(50) DEFAULT NULL,
  `category` varchar(256) DEFAULT NULL,
  `picture` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`hotel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotels`
--

LOCK TABLES `hotels` WRITE;
/*!40000 ALTER TABLE `hotels` DISABLE KEYS */;
INSERT INTO `hotels` VALUES (1,'Wyndham Grand Athens ','Megalou Alexandrou 2, Athens, 10437, Greece','+30 21 6800 9900','Just steps from Metaxourgeio Metro Station, the modern Wyndham Grand Athens boasts a rooftop outdoor pool and bar-restaurant with panoramic views over the Acropolis.',5,'Hotel','images_hotels/HotelWyndhamGrandAthens.jpg');
INSERT INTO `hotels` VALUES (2,'Praya Palazzo','757/1 Somdej Prakinklao Soi 2, Bangyeekhan, 10700 Bangkok, Thailand','+66 2 883 2998','Praya Palazzo is an elegant mansion by the Chao Phraya River, a graceful oasis in the middle of busy Bangkok City. With free Wi-Fi, it also has an outdoor pool and art gallery.',4,'Hotel','images_hotels/PrayaPalazzoBangkok.jpg');
INSERT INTO `hotels` VALUES (3,'Ataalaya Farmhouse','Sitio Cabagan, Barangay Sta. Cruz Alitagtag, Lipa, Philippines','+23 45 7656 87','Spread across 5 hectares of rural land, Ataalaya Farmhouse offers a tranquil accommodation situated atop a cliff surrounded by lush greenery. Featuring beautifully-landscaped gardens, it is 27 km from Tagaytay. Free private parking is available on site.',3,'Hostel','images_hotels/AtaalayaLipa.jpg');
INSERT INTO `hotels` VALUES (4,'The Royal Park Hotel Kyoto Sanjo','604-8004 Kyoto, Kyoto, Nakagyo-ku Sanjo-dori Kawaramachi Higashi-iru, Japan','+89 678 5678 8','The Royal Park Hotel Kyoto Sanjo features stylish modern accommodations with free wired internet access. The hotel offers massages and concierge services, as well as a bar and a buffet restaurant with savory dishes. Kyoto Shiyakusho-mae Subway Station and Sanjo Keihan Station are both within a 3-minute walk away.',4,'Hotel','images_hotels/RoyalParkKyoto.jpg');
INSERT INTO `hotels` VALUES (5,'Terrass\'\' Hôtel Montmartre by MH ','12-14 Rue Joseph De Maistre, 18th arr., 75018 Paris, France','+10 907 5678 87','Le Terrass” Hôtel is set in a 19th century building in the heart of the Montmartre district. Just a 15-minute walk from the Sacré-Coeur Basilica, the 4-star hotel offers elegantly decorated rooms with air conditioning.',5,'Hotel','images_hotels/TerrassParis.jpg');
INSERT INTO `hotels` VALUES (6,'Casa Serena','Località San Giorgio 2B, 53036 Poggibonsi, Italy','+12 4986 46 754','Overlooking the Tuscan hills, Casa Serena features rustic-style apartments with ancient furnishings, wood beamed ceilings and terracotta floors. It offers a panoramic pool and a spacious garden.',3,'Apartment','images_hotels/CasaSerenaTuscany.jpg');
INSERT INTO `hotels` VALUES (7,'Kempinski Nile Hotel, Cairo','Cornich El Nile, 12 Ahmed Ragheb Street, Garden City, Garden City, Cairo, Egypt','+5677 352 789 1','Situated in Cairo’s affluent Garden City district, Kempinski Hotel offers luxurious rooms on the shores of the Nile River. It features a wellness centre and a rooftop swimming pool.',4,'Hotel','images_hotels/KempinskiCairo.jpg');
INSERT INTO `hotels` VALUES (8,'Royal Cliff Beach Hotel ','353 Phra Tamnuk Road, South Pattaya, Banglamung, Chonburi, Bali , Thailand','+97 56 122342 5','Luxurious beach vacations await within the exclusive grounds of Royal Cliff Beach Hotel. This award-winning 5-star resort offers mountain or sea view rooms, 11 dining options and several outdoor pools with sunken bars.',5,'Hotel','images_hotels/RoyalCliffBaliThailand.jpg');
/*!40000 ALTER TABLE `hotels` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-18 17:03:49
