-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: booking_tip
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_login`
--

DROP TABLE IF EXISTS `user_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_login` (
  `login_id` int(50) NOT NULL AUTO_INCREMENT,
  `user_id` int(50) NOT NULL,
  `token` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`login_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_login_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_login`
--

LOCK TABLES `user_login` WRITE;
/*!40000 ALTER TABLE `user_login` DISABLE KEYS */;
INSERT INTO `user_login` VALUES (1,9,'anamaria.sova@yahoo.ro');
INSERT INTO `user_login` VALUES (2,9,'anamaria.sova@yahoo.ro');
INSERT INTO `user_login` VALUES (3,9,'anamaria.sova@yahoo.ro');
INSERT INTO `user_login` VALUES (4,9,'anamaria.sova@yahoo.ro');
INSERT INTO `user_login` VALUES (5,9,'anamaria.sova@yahoo.ro');
INSERT INTO `user_login` VALUES (6,9,'anamaria.sova@yahoo.ro');
INSERT INTO `user_login` VALUES (7,9,'anamaria.sova@yahoo.ro');
INSERT INTO `user_login` VALUES (8,9,'anamaria.sova@yahoo.ro');
INSERT INTO `user_login` VALUES (9,9,'anamaria.sova@yahoo.ro');
INSERT INTO `user_login` VALUES (10,9,'anamaria.sova@yahoo.ro');
INSERT INTO `user_login` VALUES (11,9,'anamaria.sova@yahoo.ro');
INSERT INTO `user_login` VALUES (12,9,'anamaria.sova@yahoo.ro');
INSERT INTO `user_login` VALUES (13,9,'anamaria.sova@yahoo.ro');
INSERT INTO `user_login` VALUES (14,9,'anamaria.sova@yahoo.ro');
INSERT INTO `user_login` VALUES (15,9,'anamaria.sova@yahoo.ro');
INSERT INTO `user_login` VALUES (16,9,'anamaria.sova@yahoo.ro');
INSERT INTO `user_login` VALUES (17,9,'anamaria.sova@yahoo.ro');
INSERT INTO `user_login` VALUES (18,9,'anamaria.sova@yahoo.ro');
INSERT INTO `user_login` VALUES (19,9,'anamaria.sova@yahoo.ro');
INSERT INTO `user_login` VALUES (20,9,'anamaria.sova@yahoo.ro');
INSERT INTO `user_login` VALUES (21,9,'anamaria.sova@yahoo.ro');
INSERT INTO `user_login` VALUES (22,14,'ioanaraluca.sova@yahoo.com');
/*!40000 ALTER TABLE `user_login` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-18 17:03:49
