create table Flights (flight_id int primary key auto_increment, plane_id int,
source_flight varchar(30), destination_flight varchar(30),
source_aeroport varchar(30), destination_aeroport varchar(30),
leave_date date, leave_hour varchar(30),arrive_date date, arrive_hour varchar(30),
available_seats int, price_seat double ,
FOREIGN KEY (plane_id) REFERENCES Planes(plane_id) on delete cascade);

insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (1,'Bucuresti','Roma','Otopeni','Ciampino','2018-05-25','18:30','2018-05-25','20:30',40,500);
insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (3,'Iasi','Atena','Aeroportul International','Eleftherios Venizelos','2018-05-25','15:00','2018-05-25','17:50',42,350);
insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (2,'Iasi','Atena','Aeroportul International','Eleftherios Venizelos','2018-05-25','12:00','2018-05-25','15:50',45,320);
insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (6,'Bucuresti','Roma','Otopeni','Ciampinos','2018-05-25','06:00','2018-05-25','08:50',44,400);
insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (4,'Bucuresti','Roma','Otopeni','Ciampino','2018-05-26','10:30','2018-05-26','12:30',40,360);
insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (3,'Bucuresti','Roma','Otopeni','Ciampino','2018-05-26','15:00','2018-05-26','17:50',42,250);
insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (5,'Iasi','Atena','Aeroportul International','Eleftherios Venizelos','2018-05-26','12:00','2018-05-26','15:50',42,500);
insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (4,'Iasi','Atena','Aeroportul International','Eleftherios Venizelos','2018-05-26','18:00','2018-05-26','20:50',40,350);

insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (5,'Bucuresti','Roma','Otopeni','Ciampino','2018-05-25','18:30','2018-05-25','20:30',40,500);
insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (6,'Iasi','Atena','Aeroportul International','Eleftherios Venizelos','2018-05-25','15:00','2018-05-25','17:50',42,350);
insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (4,'Iasi','Atena','Aeroportul International','Eleftherios Venizelos','2018-05-25','12:00','2018-05-25','15:50',45,320);
insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (1,'Bucuresti','Roma','Otopeni','Ciampinos','2018-05-25','06:00','2018-05-25','08:50',44,400);
insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (3,'Bucuresti','Roma','Otopeni','Ciampino','2018-05-26','10:30','2018-05-26','12:30',40,360);
insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (6,'Bucuresti','Roma','Otopeni','Ciampino','2018-05-26','15:00','2018-05-26','17:50',42,250);
insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (2,'Iasi','Atena','Aeroportul International','Eleftherios Venizelos','2018-05-26','12:00','2018-05-26','15:50',42,500);
insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (4,'Iasi','Atena','Aeroportul International','Eleftherios Venizelos','2018-05-26','18:00','2018-05-26','20:50',40,350);

insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (5,'Bucuresti','Roma','Otopeni','Ciampino','2018-05-25','16:30','2018-05-25','18:30',40,500);
insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (6,'Iasi','Atena','Aeroportul International','Eleftherios Venizelos','2018-05-25','12:00','2018-05-25','15:50',42,350);
insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (4,'Iasi','Atena','Aeroportul International','Eleftherios Venizelos','2018-05-25','06:00','2018-05-25','09:50',45,320);
insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (1,'Bucuresti','Roma','Otopeni','Ciampinos','2018-05-25','07:45','2018-05-25','09:30',44,400);
insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (3,'Bucuresti','Roma','Otopeni','Ciampino','2018-05-26','10:30','2018-05-26','12:30',40,360);
insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (6,'Bucuresti','Roma','Otopeni','Ciampino','2018-05-26','11:00','2018-05-26','12:50',42,250);
insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (2,'Iasi','Atena','Aeroportul International','Eleftherios Venizelos','2018-05-26','14:00','2018-05-26','15:50',42,500);
insert into Flights(plane_id,source_flight,destination_flight,source_aeroport,destination_aeroport,leave_date,leave_hour,arrive_date,arrive_hour,available_seats,price_seat) 
values (4,'Iasi','Atena','Aeroportul International','Eleftherios Venizelos','2018-05-26','23:00','2018-05-27','02:50',40,350);

