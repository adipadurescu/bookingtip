create table Planes (plane_id int primary key auto_increment,
company varchar(30), capacity int , logo varchar(30));



insert into Planes(company, capacity,logo) values ('Tarom',40,'images_airline/tarom.jpg');
insert into Planes(company, capacity,logo) values ('Blue Air',45,'images_airline/blue_air.png');
insert into Planes(company, capacity,logo) values ('Emirates',42,'images_airline/emirates.png');
insert into Planes(company, capacity,logo) values('Austrian',40,'images_airline/austrian.png');
insert into Planes(company,capacity,logo) values ('Balkan',42,'images_airline/balkan.jpg');
insert into Planes (company, capacity, logo) values('Aegean',44,'images_airline/aegean.png');