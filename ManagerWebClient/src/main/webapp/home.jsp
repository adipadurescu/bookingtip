<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>



<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Travel Booking</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="author" content="" />

<!-- Facebook and Twitter integration -->
<meta name="twitter:title" content="" />
<meta name="twitter:image" content="" />
<meta name="twitter:url" content="" />
<meta name="twitter:card" content="" />

<link
	href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700"
	rel="stylesheet">

<!-- Animate.css -->
<link rel="stylesheet" href="css/animate.css">
<!-- Icomoon Icon Fonts-->
<link rel="stylesheet" href="css/icomoon.css">
<!-- Bootstrap  -->
<link rel="stylesheet" href="css/bootstrap.css">

<!-- Magnific Popup -->
<link rel="stylesheet" href="css/magnific-popup.css">

<!-- Flexslider  -->
<link rel="stylesheet" href="css/flexslider.css">

<!-- Owl Carousel -->
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">

<!-- Date Picker -->
<link rel="stylesheet" href="css/bootstrap-datepicker.css">
<!-- Flaticons  -->
<link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

<!-- Theme style  -->
<link rel="stylesheet" href="css/style.css">

<!-- Modernizr JS -->
<script src="js/modernizr-2.6.2.min.js"></script>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>
<style>
#hotels, #profile {
	background-color: Transparent;
	border: none;
	outline: none;
}
</style>
<body>

	<div class="colorlib-loader"></div>

	<div id="page">
		<nav class="colorlib-nav" role="navigation">
			<div class="top-menu">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xs-2">
							<div id="colorlib-logo">
								<a href="home.jsp">Travel Booking</a>
							</div>
						</div>
						<div class="col-xs-10 text-right menu-1">
							<ul>
								<li class="active"><a href="home.jsp">Home</a></li>
								
								<li>
									<form style="display: inline;" action="PrintHotelsServlet"
										method="get">
										<button id="hotels">
											<a>Hotels</a>
										</button>
									</form>
								</li>
								<li><form style="display: inline;" action="ProfileServlet"
										method="post">
										<button id="profile">
											<a>Profile</a>
										</button>
									</form>
								</li>
								
							</ul>
						</div>
					</div>
				</div>
			</div>
		</nav>
		<aside id="colorlib-hero">
			<div class="flexslider">
				<ul class="slides">
					<li style="background-image: url(images/img_bg_1.jpg);">
						<div class="overlay"></div>
						<div class="container-fluid">
							<div class="row">
								<div
									class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
									<div class="slider-text-inner text-center">
										<h2>Plan your holiday and visit</h2>
										<h1>Amazing Maldives</h1>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li style="background-image: url(images/img_bg_2.jpg);">
						<div class="overlay"></div>
						<div class="container-fluid">
							<div class="row">
								<div
									class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
									<div class="slider-text-inner text-center">
										<h2>We have trips</h2>
										<h1>All around the world</h1>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li style="background-image: url(images/img_bg_5.jpg);">
						<div class="overlay"></div>
						<div class="container-fluids">
							<div class="row">
								<div
									class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
									<div class="slider-text-inner text-center">
										<h2>Explore our most tavel agency</h2>
										<h1>Travel Booking</h1>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li style="background-image: url(images/img_bg_4.jpg);">
						<div class="overlay"></div>
						<div class="container-fluid">
							<div class="row">
								<div
									class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
									<div class="slider-text-inner text-center">
										<h2>Experience the</h2>
										<h1>Best Trip Ever</h1>
									</div>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</aside>
		<div id="colorlib-reservation">
			<!-- <div class="container"> -->
			<div class="row">
				<div class="search-wrap">
					<div class="container">
						<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#flight"><i
									class="flaticon-plane"></i> Flight</a></li>
							<li><a data-toggle="tab" href="#hotel"><i
									class="flaticon-resort"></i> Hotel</a></li>
						</ul>
					</div>
						<div class="tab-content">
						<div id="flight" class="tab-pane fade in active">
							<form method="post" action="PrintFlightsServlet" class="colorlib-form">
								<div class="row">
									<div class="col-md-3">
										<div class="form-group">
											<label for="date">From:</label>
											<div class="form-field">
												<input type="text" id="location" class="form-control"
													placeholder="Search Location" name="source">
											</div>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label for="date">To:</label>
											<div class="form-field">
												<input type="text" id="location" class="form-control"
													placeholder="Search Location" name="destination">
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label for="date">Check-in:</label>
											<div class="form-field">
												<i class="icon icon-calendar2"></i> <input type="text"
													id="date" class="form-control date"
													placeholder="Check-in date" name="checkIn">
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label for="date">Check-out:</label>
											<div class="form-field">
												<i class="icon icon-calendar2"></i> <input type="text"
													id="date" class="form-control date"
													placeholder="Check-out date" name="checkOut">
											</div>
										</div>
									</div>
									<div class="col-md-3" style="width:260px">
										<div class="form-group">
											<label for="guests" >Guest</label>
											<div class="form-field">
												<i class="icon icon-arrow-down3"></i> <select name="people"
													id="people" class="form-control">
													<option value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">5</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<input type="submit" name="submit" id="submit"
											value="Find Flights" class="btn btn-primary btn-block">
									</div>
								</div>
							</form>
						</div>
						<div id="hotel" class="tab-pane fade">
							<form method="post" action="PrintHotelsServlet"
								class="colorlib-form">
								<div class="row">
									<div class="col-md-2">
										<div class="booknow">
											<h2>Book Now</h2>
											<span>Best Price Online</span>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label for="date">Where:</label>
											<div class="form-field">
												<input type="text" id="location" class="form-control"
													placeholder="Search Location" name="location">
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label for="date">Check-in:</label>
											<div class="form-field">
												<i class="icon icon-calendar2"></i> <input type="text"
													id="date" class="form-control date"
													placeholder="Check-in date" name="checkIn">
											</div>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label for="date">Check-out:</label>
											<div class="form-field">
												<i class="icon icon-calendar2"></i> <input type="text"
													id="date" class="form-control date"
													placeholder="Check-out date" name="checkOut">
											</div>
										</div>
									</div>

									<div class="col-md-2">
										<input type="submit" name="submit" id="submit"
											value="Find Hotel" class="btn btn-primary btn-block">
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="colorlib-services">
		<div class="container">
			<div class="row no-gutters">
				<div class="col-md-3 animate-box text-center aside-stretch">
					<div class="services">
						<span class="icon"> <i class="flaticon-around"></i>
						</span>
						<h3>Amazing Travel</h3>
						<p>Separated they live in Bookmarksgrove right at the coast of
							the Semantics, a large language ocean. A small river named Duden
							flows by their place and supplies</p>
					</div>
				</div>
				<div class="col-md-3 animate-box text-center">
					<div class="services">
						<span class="icon"> <i class="flaticon-resort"></i>
						</span>
						<h3>Our Hotels</h3>
						<p>Separated they live in Bookmarksgrove right at the coast of
							the Semantics, a large language ocean. A small river named Duden
							flows by their place and supplies</p>
					</div>
				</div>
				<div class="col-md-3 animate-box text-center">
					<div class="services">
						<span class="icon"> <i class="flaticon-postcard"></i>
						</span>
						<h3>Nice Support</h3>
						<p>Separated they live in Bookmarksgrove right at the coast of
							the Semantics, a large language ocean. A small river named Duden
							flows by their place and supplies</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="colorlib-tour colorlib-light-grey">
		<div class="container">
			<div class="row">
				<div
					class="col-md-6 col-md-offset-3 text-center colorlib-heading animate-box">
					<h2>Popular Destination</h2>
					<p>We love to tell our successful far far away, behind the word
						mountains, far from the countries Vokalia and Consonantia, there
						live the blind texts.</p>
				</div>
			</div>
		</div>
		<div class="tour-wrap">
			<form action="PrintHotelsServlet" method="post"><button id="hotels" class="tour-entry animate-box">
				<div class="tour-img" style="background-image: url(images/tour-1.jpg);"></div> 
				<span class="desc">
					<h2>Athens, Greece</h2>
				</span>
				<input type="hidden" name="location" value="Athens"/>
				<input type="hidden" name="checkIn" value=""/>
				<input type="hidden" name="checkOut" value=""/>
			</button></form>
			 <form action="PrintHotelsServlet" method="post"><button id="hotels" class="tour-entry animate-box">
				<div class="tour-img" style="background-image: url(images/tour-2.jpg);"></div> 
				<span class="desc">
					<h2>Bangkok, Thailand</h2>
				</span>
				<input type="hidden" name="location" value="Bangkok"/>
				<input type="hidden" name="checkIn" value=""/>
				<input type="hidden" name="checkOut" value=""/>
			</button></form> 
			<form action="PrintHotelsServlet" method="post"><button id="hotels" class="tour-entry animate-box">
				<div class="tour-img" style="background-image: url(images/tour-3.jpg);"></div> 
				<span class="desc">
					<h2>Lipa, Philippines</h2>
				</span>
				<input type="hidden" name="location" value="Lipa"/>
				<input type="hidden" name="checkIn" value=""/>
				<input type="hidden" name="checkOut" value=""/>
			</button></form> 
			<form action="PrintHotelsServlet" method="post"><button id="hotels" class="tour-entry animate-box">
				<div class="tour-img" style="background-image: url(images/tour-4.jpg);"></div>
				<span class="desc">
					<h2>Kyoto, Japan</h2>
				</span>
				<input type="hidden" name="location" value="Kyoto"/>
				<input type="hidden" name="checkIn" value=""/>
				<input type="hidden" name="checkOut" value=""/>
			</button></form> 
			<form action="PrintHotelsServlet" method="post"><button id="hotels" class="tour-entry animate-box">
				<div class="tour-img" style="background-image: url(images/tour-5.jpg);"></div>
				<span class="desc">
					<h2>Paris, France</h2>
				</span>
				<input type="hidden" name="location" value="Paris"/>
				<input type="hidden" name="checkIn" value=""/>
				<input type="hidden" name="checkOut" value=""/>
			</button></form> 
			<form action="PrintHotelsServlet" method="post"><button id="hotels" class="tour-entry animate-box">
				<div class="tour-img" style="background-image: url(images/tour-6.jpg);"></div> 
				<span class="desc">
					<h2>Tuscany, Italy</h2>
				</span>
				<input type="hidden" name="location" value="Tuscany"/>
				<input type="hidden" name="checkIn" value=""/>
				<input type="hidden" name="checkOut" value=""/>
			</button> </form>
			<form action="PrintHotelsServlet" method="post"><button id="hotels" class="tour-entry animate-box">
				<div class="tour-img" style="background-image: url(images/tour-7.jpg);"></div>
				<span class="desc">
					<h2>Cairo, Egypt</h2>
				</span>
				<input type="hidden" name="location" value="Cairo"/>
				<input type="hidden" name="checkIn" value=""/>
				<input type="hidden" name="checkOut" value=""/>
			</button></form>
			 <form action="PrintHotelsServlet" method="post"><button id="hotels" class="tour-entry animate-box">
				<div class="tour-img" style="background-image: url(images/tour-8.jpg);"></div> 
				<span class="desc">
					<h2>Bali, Thailand</h2>
				</span>
				<input type="hidden" name="location" value="Bali"/>
				<input type="hidden" name="checkIn" value=""/>
				<input type="hidden" name="checkOut" value=""/>
			</button></form>
		</div>
	</div>




	<div id="colorlib-hotel">
		<div class="container">
			<div class="row">
				<div
					class="col-md-6 col-md-offset-3 text-center colorlib-heading animate-box">
					<h2>Recommended Hotels</h2>
					<p>We love to tell our successful far far away, behind the word
						mountains, far from the countries Vokalia and Consonantia, there
						live the blind texts.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 animate-box">
					<div class="owl-carousel">
						<div class="item">
							<div class="hotel-entry">
								<a href="PrintHotelRoomsServlet?hotel_id=1" class="hotel-img"
									style="background-image: url(images_hotels/HotelWyndhamGrandAthens.jpg);">
								</a>
								<div class="desc">
									<p class="star">
										<span><i class="icon-star-full"></i><i
											class="icon-star-full"></i><i class="icon-star-full"></i><i
											class="icon-star-full"></i><i class="icon-star-full"></i></span> 
									</p>
									<h3>
									<a href="PrintHotelRoomsServlet?hotel_id=1">Wyndham Grand Athens </a>
									</h3>
									<span class="place">Athens, Greece</span>
									<p>Enjoy a classic Athens stay.
									Central location on Karaiskaki Square near the Acropolis and the Parthenon.</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="hotel-entry">
								<a href="PrintHotelRoomsServlet?hotel_id=4" class="hotel-img"
									style="background-image: url(images_hotels/RoyalParkKyoto.jpg);">
								</a>
								<div class="desc">
									<p class="star">
										<span><i class="icon-star-full"></i><i
											class="icon-star-full"></i><i class="icon-star-full"></i><i
											class="icon-star-full"></i></span>
									</p>
									<h3>
										<a href="PrintHotelRoomsServlet?hotel_id=4">The Royal Park Hotel Kyoto Sanjo</a>
									</h3>
									<span class="place">Kyoto, Japan</span>
									<p>Stay in heart of Kyoto.This property also has one of the best-rated locations in Kyoto.
									 Guests are happier about it compared to other properties in the area.</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="hotel-entry">
								<a href="PrintHotelRoomsServlet?hotel_id=5" class="hotel-img"
									style="background-image: url(images_hotels/TerrassParis.jpg);">
								</a>
								<div class="desc">
									<p class="star">
										<span><i class="icon-star-full"></i><i
											class="icon-star-full"></i><i class="icon-star-full"></i><i
											class="icon-star-full"></i><i class="icon-star-full"></i></span> 
									</p>
									<h3>
										<a href="PrintHotelRoomsServlet?hotel_id=5">Terrass'' Hôtel Montmartre by MH </a>
									</h3>
									<span class="place">Paris, France</span>
									<p>Guests can admire the Eiffel Tower when having breakfast on the rooftop terrace.
									 Its panoramic terrace features large bay windows and offers clear views of Paris.</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="hotel-entry">
								<a href="PrintHotelRoomsServlet?hotel_id=7" class="hotel-img"
									style="background-image: url(images_hotels/KempinskiCairo.jpg);">
								</a>
								<div class="desc">
									<p class="star">
										<span><i class="icon-star-full"></i><i
											class="icon-star-full"></i><i class="icon-star-full"></i><i
											class="icon-star-full"></i></span> 
									</p>
									<h3>
										<a href="PrintHotelRoomsServlet?hotel_id=7">Kempinski Nile Hotel, Cairo</a>
									</h3>
									<span class="place">Cairo, Egipt</span>
									<p>Situated in Cairo’s affluent Garden City district, Kempinski Hotel offers 
									luxurious rooms on the shores of the Nile River.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div id="colorlib-subscribe"
		style="background-image: url(images/img_bg_2.jpg);"
		data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div
					class="col-md-6 col-md-offset-3 text-center colorlib-heading animate-box">
					<h2>Sign Up for a Newsletter</h2>
					<p>Sign up for our mailing list to get latest updates and
						offers.</p>
					<form class="form-inline qbstp-header-subscribe">
						<div class="row">
							<div class="col-md-12 col-md-offset-0">
								<div class="form-group">
									<input type="text" class="form-control" id="email"
										placeholder="Enter your email">
									<button type="submit" class="btn btn-primary">Subscribe</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<footer id="colorlib-footer" role="contentinfo">
		<div class="container">
			<div class="row row-pb-md">
				<div class="col-md-3 colorlib-widget">
					<h4>Tour Agency</h4>
					<p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia
						reprehenderit. Eos cumque dicta adipisci architecto culpa amet.</p>
					<p>
					<ul class="colorlib-social-icons">
						<li><a href="#"><i class="icon-twitter"></i></a></li>
						<li><a href="#"><i class="icon-facebook"></i></a></li>
						<li><a href="#"><i class="icon-linkedin"></i></a></li>
						<li><a href="#"><i class="icon-dribbble"></i></a></li>
					</ul>


				</div>
				<div class="col-md-2 colorlib-widget">
					<h4>Book Now</h4>
					<p>
					<ul class="colorlib-footer-links">
						<li><a href="#">Flight</a></li>
						<li><a href="#">Hotels</a></li>
					</ul>
					</p>

				</div>
				<div class="col-md-2 colorlib-widget">
					<h4>Top Deals</h4>
					<p>
					<ul class="colorlib-footer-links">
						<li><a href="#">Edina Hotel</a></li>
						<li><a href="#">Quality Suites</a></li>
						<li><a href="#">The Hotel Zephyr</a></li>
						<li><a href="#">Da Vinci Villa</a></li>
						<li><a href="#">Hotel Epikk</a></li>
					</ul>
					</p>

				</div>

				<div class="col-md-3 col-md-push-1">
					<h4>Contact Information</h4>
					<ul class="colorlib-footer-links">
						<li>291 South 21th Street, <br> Suite 721 New York NY
							10016
						</li>
						<li><a href="tel://1234567920">+ 1235 2355 98</a></li>
						<li><a href="mailto:info@yoursite.com">info@yoursite.com</a></li>
						<li><a href="#">yoursite.com</a></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<p>
						Copyright &copy;
						<script>
							document.write(new Date().getFullYear());
						</script>
						All rights reserved
					</p>
				</div>
			</div>
		</div>
	</footer>
	


	<div class="gototop js-top">
		<a href="#" class="gotop"><i class="icon-arrow-up2"></i></a> 
	</div> 
	<script type="text/javascript">

		$('.gotop').on('click', function(event){
			
			event.preventDefault();

			$('html, body').animate({
				scrollTop: $('html').offset().top
			}, 500, 'easeInOutExpo');
			
			return false;
		});

		$(window).scroll(function(){

			var $win = $(window);
			if ($win.scrollTop() > 200) {
				$('.js-top').addClass('active');
			} else {
				$('.js-top').removeClass('active');
			}

		});
	
	</script>

	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Flexslider -->
	<script src="js/jquery.flexslider-min.js"></script>
	<!-- Owl carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<!-- Date Picker -->
	<script src="js/bootstrap-datepicker.js"></script>
	<!-- Stellar Parallax -->
	<script src="js/jquery.stellar.min.js"></script>
	<!-- Main -->
	<script src="js/main.js"></script>

</body>
</html>

