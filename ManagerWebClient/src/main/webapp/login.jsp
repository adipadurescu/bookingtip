<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="author" content="" />

<!-- Facebook and Twitter integration -->
<meta name="twitter:title" content="" />
<meta name="twitter:image" content="" />
<meta name="twitter:url" content="" />
<meta name="twitter:card" content="" />

<link
	href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700"
	rel="stylesheet">

<!-- Animate.css -->
<link rel="stylesheet" href="css/animate.css">
<!-- Icomoon Icon Fonts-->
<link rel="stylesheet" href="css/icomoon.css">
<!-- Bootstrap  -->
<link rel="stylesheet" href="css/bootstrap.css">

<!-- Magnific Popup -->
<link rel="stylesheet" href="css/magnific-popup.css">

<!-- Flexslider  -->
<link rel="stylesheet" href="css/flexslider.css">

<!-- Owl Carousel -->
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">

<!-- Date Picker -->
<link rel="stylesheet" href="css/bootstrap-datepicker.css">
<!-- Flaticons  -->
<link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

<!-- Theme style  -->
<link rel="stylesheet" href="css/style.css">

<!-- Modernizr JS -->
<script src="js/modernizr-2.6.2.min.js"></script>
<!-- FOR IE9 below -->
<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>
<style>body{ background: url("images/img_bg_2.jpg")no-repeat center center fixed; background-size: cover;
	background-repeat: no-repeat; }</style>
<body>
	<!-- SIDEBAR-->
	<div class="col-md-3">
		<div class="sidebar-wrap">
			<div class="side search-wrap animate-box" style="margin-left:10%; margin-top:20%;">
				<h3 class="sidebar-heading" align="center">Log in</h3>
				<form action="LoginServlet" method="post" class="colorlib-form">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="date">Email:</label>
								<div class="form-field">
								 <input type="text" id="date" class="form-control" name="email">
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label for="date">Password:</label>
								<div class="form-field">
									<input type="password" id="date" class="form-control" name="password">
								</div>
							</div>
						</div>
						<div align="center"><h4><a href="register.jsp">Not registered? Click here.</a></h4></div>
						<div align="center"><h4><a href="forgotPassword.jsp">Forgot your password? Click here.</a></h4></div>
						<h3 align=center style="color:white">${status}</h3>
						<div class="col-md-12">
				              <input type="submit" name="submit" id="submit" value="Log in" class="btn btn-primary btn-block">
				        </div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Flexslider -->
	<script src="js/jquery.flexslider-min.js"></script>
	<!-- Owl carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<!-- Date Picker -->
	<script src="js/bootstrap-datepicker.js"></script>
	<!-- Stellar Parallax -->
	<script src="js/jquery.stellar.min.js"></script>
	<!-- Main -->
	<script src="js/main.js"></script>
</body>
</html>