package ro.h23.manager.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ro.h23.manager.core.flight.Flight;
import ro.h23.manager.core.flight.FlightDAO;
import ro.h23.manager.core.flight.Plane;
import ro.h23.manager.core.flight.ReservationFlight;

public class FlightDAOImpl implements FlightDAO {

	@Override
	public Connection getConnection() {

		Connection conn = null;

		System.out.println("-------- MySql Connection Testing ------");

		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "adi", "");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}

	@Override
	public List<Flight> getFlightsByUserPreferences(String source, String destination, String leave, String arrive) {

		List<Flight> flights = new ArrayList<Flight>();
		Connection con = getConnection();
		String sql = "select f.flight_id,f.plane_id,p.capacity,p.company, p.logo, f.source_flight, f.destination_flight, f.source_aeroport, f.destination_aeroport, f.leave_date, f.leave_hour,f.arrive_date,f.arrive_hour, f.available_seats, f.price_seat from flights f, Planes p where f.plane_id=p.plane_id and f.source_flight=? and f.destination_flight=? and f.leave_date=? and f.arrive_date=? and f.available_seats>0";
		ResultSet rs = null;
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = con.prepareStatement(sql);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			preparedStatement.setString(1, source);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			preparedStatement.setString(2, destination);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			preparedStatement.setString(3,leave);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			preparedStatement.setString(4, arrive);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			rs = preparedStatement.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			while (rs.next()) {
				Flight f = new Flight();
				f.setFlight_id(rs.getInt("flight_id"));
				f.setPlane(new Plane(rs.getInt("plane_id"), rs.getString("company"), rs.getInt("capacity"),
						rs.getString("logo")));
				f.setArrive_date(rs.getString("arrive_date"));
				f.setArrive_hour(rs.getString("arrive_hour"));
				f.setLeave_date(rs.getString("leave_date"));
				f.setLeave_hour(rs.getString("leave_hour"));
				f.setAvailable_seats(rs.getInt("available_seats"));
				f.setDestination_aeropot(rs.getString("destination_aeroport"));
				f.setSource_aeroport(rs.getString("source_aeroport"));
				f.setPrice_seat(rs.getDouble("price_seat"));
				f.setSource_flight(rs.getString("source_flight"));
				f.setDestination_flight(rs.getString("destination_flight"));
				flights.add(f);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return flights;

	}

	public List<Flight> getFlightsByCompany(String source, String destination, String leave, String arrive,
			String company) {
		System.out.println(source+" "+destination+" "+leave+" "+arrive+" "+company);
		List<Flight> flights = new ArrayList<Flight>();
		Connection con = getConnection();
		String sql = "select f.flight_id,f.plane_id,p.capacity,p.company, p.logo, f.source_flight, f.destination_flight, f.source_aeroport, f.destination_aeroport, f.leave_date, f.leave_hour,f.arrive_date,f.arrive_hour, f.available_seats, f.price_seat from flights f, Planes p where f.plane_id=p.plane_id and f.source_flight=? and f.destination_flight=? and p.company = ? and f.leave_date=? and f.arrive_date=? and f.available_seats>0";
		ResultSet rs = null;
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = con.prepareStatement(sql);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			preparedStatement.setString(1, source);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			preparedStatement.setString(2, destination);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			preparedStatement.setString(3, company);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			preparedStatement.setString(4, leave);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			preparedStatement.setString(5, arrive);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			rs = preparedStatement.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			while (rs.next()) {
				Flight f = new Flight();
				f.setFlight_id(rs.getInt("flight_id"));
				f.setPlane(new Plane(rs.getInt("plane_id"), rs.getString("company"), rs.getInt("capacity"),
						rs.getString("logo")));
				f.setArrive_date(rs.getString("arrive_date"));
				f.setArrive_hour(rs.getString("arrive_hour"));
				f.setLeave_date(rs.getString("leave_date"));
				f.setLeave_hour(rs.getString("leave_hour"));
				f.setAvailable_seats(rs.getInt("available_seats"));
				f.setDestination_aeropot(rs.getString("destination_aeroport"));
				f.setSource_aeroport(rs.getString("source_aeroport"));
				f.setPrice_seat(rs.getDouble("price_seat"));
				f.setSource_flight(rs.getString("source_flight"));
				f.setDestination_flight(rs.getString("destination_flight"));
				flights.add(f);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return flights;

	}
	
	public int createReservationFlight(ReservationFlight reservationFlight) {
		int status = 0;
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("insert into ReservationsFlights(user_id,flight_id,price_seats,number_seats,leave_date,leave_hour, arrive_date, arrive_hour) values( ?,?,?,?,?,?,?,?)");

			ps.setInt(1, reservationFlight.getUser_id());
			ps.setInt(2, reservationFlight.getFlight_id());
			ps.setDouble(3,reservationFlight.getPrice_reservation());
			ps.setInt(4, reservationFlight.getNumber_seats());
			ps.setString(5, reservationFlight.getLeave_date());	
			ps.setString(6, reservationFlight.getLeave_hour());
			ps.setString(7, reservationFlight.getArrive_date());	
			ps.setString(8, reservationFlight.getArrive_hour());
			
			status = ps.executeUpdate();
			
			con.close();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}
	
	public String getUserName(int id) {
		String name = "";
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("select upper(first_name) || ' ' || upper(last_name) as name from user where user_id=?");

			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				name = rs.getString("name");
			}
			
			con.close();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return name;
	}

	@Override
	public void updateSeatsAvailable(int id_flight, int seats_reservation) {
		System.out.println("1---id_flight******" + id_flight);
		int number_seats=getSeatsAvailable(id_flight);
		number_seats-=seats_reservation;
		Connection con = getConnection();
		String sql = "update Flights set available_seats=? where flight_id=?";
		ResultSet rs = null;
		PreparedStatement preparedStatement = null;

		try {
			preparedStatement = con.prepareStatement(sql);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			preparedStatement.setInt(1,number_seats);
			preparedStatement.setInt(2,id_flight);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public int getSeatsAvailable(int id_flight) {
		System.out.println("id_flight******" + id_flight);
		Connection con = getConnection();
		String sql = "Select available_seats from Flights where flight_id=?";
		ResultSet rs = null;
		PreparedStatement preparedStatement = null;
		int number_seats=0;
		try {
			preparedStatement = con.prepareStatement(sql);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			preparedStatement.setInt(1, id_flight);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			rs = preparedStatement.executeQuery();
			if(rs.next())
			{
				number_seats=rs.getInt("available_seats");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("number_seats******" + number_seats);
		return number_seats;
	}

	@Override
	public Flight getInfoFlight(int id_flight) {
		//List<Flight> flights = new ArrayList<Flight>();
		Connection con = getConnection();
		String sql = "Select * from Flights f, Planes p where f.flight_id=? and f.plane_id=p.plane_id";
		ResultSet rs = null;
		Flight f = new Flight();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = con.prepareStatement(sql);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			preparedStatement.setInt(1, id_flight);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			rs = preparedStatement.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			while (rs.next()) {
				
				f.setFlight_id(rs.getInt("flight_id"));
				f.setPlane(new Plane(rs.getInt("plane_id"), rs.getString("company"), rs.getInt("capacity"),
						rs.getString("logo")));
				f.setArrive_date(rs.getString("arrive_date"));
				f.setArrive_hour(rs.getString("arrive_hour"));
				f.setLeave_date(rs.getString("leave_date"));
				f.setLeave_hour(rs.getString("leave_hour"));
				f.setAvailable_seats(rs.getInt("available_seats"));
				f.setDestination_aeropot(rs.getString("destination_aeroport"));
				f.setSource_aeroport(rs.getString("source_aeroport"));
				f.setPrice_seat(rs.getDouble("price_seat"));
				f.setSource_flight(rs.getString("source_flight"));
				f.setDestination_flight(rs.getString("destination_flight"));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return f;
	}
}
