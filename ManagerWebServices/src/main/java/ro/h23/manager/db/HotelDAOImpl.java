package ro.h23.manager.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ro.h23.manager.core.hotel.Hotel;
import ro.h23.manager.core.hotel.HotelDAO;

public class HotelDAOImpl implements HotelDAO {

	private static HotelDAOImpl instance = new HotelDAOImpl();

	public static HotelDAOImpl instance() {
		return instance;
	}

	public HotelDAOImpl() {

	}

	@Override
	public Connection getConnection() {
		Connection conn = null;

		System.out.println("-------- MySql Connection Testing ------");

		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "adi", "");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}

	public List<Hotel> getAllHotels() {
		List<Hotel> hotels = new ArrayList<Hotel>();
		Connection con = getConnection();
		try (Statement st = con.createStatement()) {
			st.execute("select * from hotels");
			ResultSet rs = st.getResultSet();
			while (rs.next()) {
				Hotel h = new Hotel();
				h.setHotel_id(rs.getInt("hotel_id"));
				h.setName(rs.getString("name"));
				h.setLocation(rs.getString("location"));
				h.setPhone_number(rs.getString("phone_number"));
				h.setDescription(rs.getString("description"));
				h.setStar_rating(rs.getInt("star_rating"));
				h.setCategory(rs.getString("category"));
				h.setPicture(rs.getString("picture"));
				hotels.add(h);
			}
			return hotels;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<Hotel> getHotelsByUserPreferences(String location) {
		List<Hotel> hotels = new ArrayList<Hotel>();
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("select * from hotels where location like ?");
			ps.setString(1, "%" + location + "%");

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Hotel h = new Hotel();
				h.setHotel_id(rs.getInt("hotel_id"));
				h.setName(rs.getString("name"));
				h.setLocation(rs.getString("location"));
				h.setPhone_number(rs.getString("phone_number"));
				h.setDescription(rs.getString("description"));
				h.setStar_rating(rs.getInt("star_rating"));
				h.setCategory(rs.getString("category"));
				h.setPicture(rs.getString("picture"));
				hotels.add(h);
			}
			return hotels;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getHotelNameById(int id) {
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("select name from hotels where hotel_id=?");
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString("name");
			}
		} catch (SQLException e) {
			e.printStackTrace();

		}
		return null;
	}
	
	public List<Hotel> getHotelsByMinMaxPriceWithLocation(String location,int minPrice,int maxPrice){
		List<Hotel> hotels = new ArrayList<Hotel>();
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("select * from hotels where location like ? and hotel_id in (select hotel_id from hotel_rooms where price >=? and price <=?)");
			ps.setString(1, "%" + location + "%");
			ps.setInt(2, minPrice);
			ps.setInt(3, maxPrice);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Hotel h = new Hotel();
				h.setHotel_id(rs.getInt("hotel_id"));
				h.setName(rs.getString("name"));
				h.setLocation(rs.getString("location"));
				h.setPhone_number(rs.getString("phone_number"));
				h.setDescription(rs.getString("description"));
				h.setStar_rating(rs.getInt("star_rating"));
				h.setCategory(rs.getString("category"));
				h.setPicture(rs.getString("picture"));
				hotels.add(h);
			}
			return hotels;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Hotel> getHotelsByMinMaxPrice(int minPrice,int maxPrice){
		List<Hotel> hotels = new ArrayList<Hotel>();
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("select * from hotels where hotel_id in (select hotel_id from hotel_rooms where price >=? and price <=?)");
			ps.setInt(1, minPrice);
			ps.setInt(2, maxPrice);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Hotel h = new Hotel();
				h.setHotel_id(rs.getInt("hotel_id"));
				h.setName(rs.getString("name"));
				h.setLocation(rs.getString("location"));
				h.setPhone_number(rs.getString("phone_number"));
				h.setDescription(rs.getString("description"));
				h.setStar_rating(rs.getInt("star_rating"));
				h.setCategory(rs.getString("category"));
				h.setPicture(rs.getString("picture"));
				hotels.add(h);
			}
			return hotels;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Hotel> getHotelsByStarRatingWithLocation(String location,int starRating){
		List<Hotel> hotels = new ArrayList<Hotel>();
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("select * from hotels where location like ? and star_rating=?");
			ps.setString(1, "%" + location + "%");
			ps.setInt(2, starRating);


			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Hotel h = new Hotel();
				h.setHotel_id(rs.getInt("hotel_id"));
				h.setName(rs.getString("name"));
				h.setLocation(rs.getString("location"));
				h.setPhone_number(rs.getString("phone_number"));
				h.setDescription(rs.getString("description"));
				h.setStar_rating(rs.getInt("star_rating"));
				h.setCategory(rs.getString("category"));
				h.setPicture(rs.getString("picture"));
				hotels.add(h);
			}
			return hotels;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Hotel> getHotelsByStarRating(int starRating){
		List<Hotel> hotels = new ArrayList<Hotel>();
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("select * from hotels where star_rating=?");
			ps.setInt(1, starRating);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Hotel h = new Hotel();
				h.setHotel_id(rs.getInt("hotel_id"));
				h.setName(rs.getString("name"));
				h.setLocation(rs.getString("location"));
				h.setPhone_number(rs.getString("phone_number"));
				h.setDescription(rs.getString("description"));
				h.setStar_rating(rs.getInt("star_rating"));
				h.setCategory(rs.getString("category"));
				h.setPicture(rs.getString("picture"));
				hotels.add(h);
			}
			return hotels;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Hotel> getHotelsByCategoryWithLocation(String location,String category){
		List<Hotel> hotels = new ArrayList<Hotel>();
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("select * from hotels where location like ? and category=?");
			ps.setString(1, "%" + location + "%");
			ps.setString(2, category);


			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Hotel h = new Hotel();
				h.setHotel_id(rs.getInt("hotel_id"));
				h.setName(rs.getString("name"));
				h.setLocation(rs.getString("location"));
				h.setPhone_number(rs.getString("phone_number"));
				h.setDescription(rs.getString("description"));
				h.setStar_rating(rs.getInt("star_rating"));
				h.setCategory(rs.getString("category"));
				h.setPicture(rs.getString("picture"));
				hotels.add(h);
			}
			return hotels;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Hotel> getHotelsByCategory(String category){
		List<Hotel> hotels = new ArrayList<Hotel>();
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("select * from hotels where category=?");
			ps.setString(1, category);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Hotel h = new Hotel();
				h.setHotel_id(rs.getInt("hotel_id"));
				h.setName(rs.getString("name"));
				h.setLocation(rs.getString("location"));
				h.setPhone_number(rs.getString("phone_number"));
				h.setDescription(rs.getString("description"));
				h.setStar_rating(rs.getInt("star_rating"));
				h.setCategory(rs.getString("category"));
				h.setPicture(rs.getString("picture"));
				hotels.add(h);
			}
			return hotels;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

}
