package ro.h23.manager.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ro.h23.manager.core.hotel.CheckAvailableRoom;
import ro.h23.manager.core.hotel.HotelRoom;
import ro.h23.manager.core.hotel.HotelRoomDAO;
import ro.h23.manager.core.hotel.Reservation;

public class HotelRoomDAOImpl implements HotelRoomDAO {

	private static HotelRoomDAOImpl instance = new HotelRoomDAOImpl();

	public static HotelRoomDAOImpl instance() {
		return instance;
	}

	public HotelRoomDAOImpl() {

	}

	@Override
	public Connection getConnection() {
		Connection conn = null;

		System.out.println("-------- MySql Connection Testing ------");

		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "adi", "");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}

	public List<HotelRoom> getHotelRoomsByHotelId(int id) {
		List<HotelRoom> hotelRooms = new ArrayList<HotelRoom>();
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("select * from hotel_rooms where hotel_id = ? group by type");
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				HotelRoom hotelRoom = new HotelRoom();
				hotelRoom.setRoom_id(rs.getInt("room_id"));
				hotelRoom.setHotel_id(rs.getInt("hotel_id"));
				hotelRoom.setRoom_number(rs.getInt("room_number"));
				hotelRoom.setPrice(rs.getInt("price"));
				hotelRoom.setType(rs.getString("type"));
				hotelRoom.setDescription(rs.getString("description"));
				hotelRoom.setPicture(rs.getString("picture"));
				hotelRooms.add(hotelRoom);
			}
			return hotelRooms;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public HotelRoom getHotelRoomsByHotelIdAndTypeAndCheckIn(CheckAvailableRoom check) {
		try {
			Connection con = getConnection(); // returns all rooms with the hotel id and type introduced by user
			PreparedStatement psRooms = con
					.prepareStatement("select room_number,hotel_id from hotel_rooms where hotel_id=? and type=?");
			psRooms.setInt(1, check.getHotelRoom().getHotel_id());
			psRooms.setString(2, check.getHotelRoom().getType());
			ResultSet rsRooms = psRooms.executeQuery();
			while (rsRooms.next()) {
				// first check if the rooms are not in the reservation table.
				PreparedStatement psCheck = con
						.prepareStatement("select * from reservations where hotel_id=? and room_number=?");
				psCheck.setInt(1, rsRooms.getInt("hotel_id"));
				System.out.println(rsRooms.getInt("hotel_id"));
				psCheck.setInt(2, rsRooms.getInt("room_number"));
				System.out.println(rsRooms.getInt("room_number"));
				ResultSet rsCheck = psCheck.executeQuery();
				if (rsCheck.next() == false) { // if the room is not in the reservation table return it
					HotelRoom hotelRoom = new HotelRoom();
					hotelRoom.setHotel_id(rsRooms.getInt("hotel_id"));
					hotelRoom.setRoom_number(rsRooms.getInt("room_number"));
					return hotelRoom;
				}
			}
			rsRooms = psRooms.executeQuery();
			while (rsRooms.next()) {
				// check available rooms. Return the first one available
				PreparedStatement ps = con.prepareStatement(
						" select * from booking_tip.reservations where ((date_format(str_to_date(?,'%m/%d/%Y'),'%Y-%m-%d')>=check_in and date_format(str_to_date(?,'%m/%d/%Y'),'%Y-%m-%d')<=check_out) or (date_format(str_to_date(?,'%m/%d/%Y'),'%Y-%m-%d')>=check_in and date_format(str_to_date(?,'%m/%d/%Y'),'%Y-%m-%d')<=check_out)) and( room_number in (select room_number from booking_tip.hotel_rooms where hotel_id = ? and type=?)and  hotel_id=?)");
				ps.setString(1, check.getCheckIn());
				ps.setString(2, check.getCheckIn());
				ps.setString(3, check.getCheckOut());
				ps.setString(4, check.getCheckOut());
				ps.setInt(5, check.getHotelRoom().getHotel_id());
				ps.setString(6, check.getHotelRoom().getType());
				ps.setInt(7, check.getHotelRoom().getHotel_id());
				ResultSet rs = ps.executeQuery();
				if (!rs.next()) {
					HotelRoom hotelRoom = new HotelRoom();
					hotelRoom.setHotel_id(rsRooms.getInt("hotel_id"));
					hotelRoom.setRoom_number(rsRooms.getInt("room_number"));
					return hotelRoom;
				}
			}

			

		} catch (

		SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public int calculateNumberOfNights(CheckAvailableRoom checkAvailableRoom) {
		try {
			Connection con = getConnection();
			PreparedStatement ps = con
					.prepareStatement(" select str_to_date(?,'%m/%d/%Y')-str_to_date(?,'%m/%d/%Y') as difference");
			ps.setString(1, checkAvailableRoom.getCheckOut());
			ps.setString(2, checkAvailableRoom.getCheckIn());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getInt("difference");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	public int getTotalPriceByTypeAndHotelId(CheckAvailableRoom checkAvailableRoom) {
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement(" select price from hotel_rooms where hotel_id=? and type=?");
			ps.setInt(1, checkAvailableRoom.getHotelRoom().getHotel_id());
			ps.setString(2, checkAvailableRoom.getHotelRoom().getType());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getInt("price") * calculateNumberOfNights(checkAvailableRoom);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	public int createReservation(Reservation reservation) {
		int status = 0;
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("insert into reservations(user_id,hotel_id,room_number,check_in,check_out,special_requests) values( ?, ?,?,date_format(str_to_date(?,'%m/%d/%Y'),'%Y-%m-%d'),date_format(str_to_date(?,'%m/%d/%Y'),'%Y-%m-%d'),?)");

			ps.setInt(1, reservation.getUser_id());
			ps.setInt(2, reservation.getHotel_id());
			ps.setInt(3,reservation.getRoom_number());
			ps.setString(4, reservation.getCheck_in());
			ps.setString(5, reservation.getCheck_out());	
			ps.setString(6, reservation.getSpecial_requests());
			status = ps.executeUpdate();
			
			con.close();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}
}
