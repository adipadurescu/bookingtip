package ro.h23.manager.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
//import java.util.Collections;
import java.util.List;

import ro.h23.manager.core.hotel.Reservation;
import ro.h23.manager.core.flight.ReservationFlight;
import ro.h23.manager.core.user.User;
import ro.h23.manager.core.user.UserDAO;

public class UserDAOImpl implements UserDAO {

	private static UserDAO instance = new UserDAOImpl();

	public static UserDAO instance() {
		return instance;
	}

	public UserDAOImpl() {

	}
	@Override
	public Connection getConnection() {
		Connection conn = null;

		System.out.println("-------- MySql Connection Testing ------");

		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test","adi","");
		} catch (SQLException e) {
			e.printStackTrace();		
		}
	return conn;
	}

	// For Registration
	public int registerUser(User user) {
		int status = 0,userExists=0;
		UserDAOImpl aux=new UserDAOImpl();
		try {
			Connection con = aux.getConnection();
			Statement st = con.createStatement();
			st.execute("select * from user");
			ResultSet rs = st.getResultSet();
			while (rs.next()) {
				if(rs.getString("email").equals(user.getEmail())) {
					userExists=1;
				}
			}
			if(userExists==0) {
				PreparedStatement ps = con.prepareStatement("insert into user(first_name,last_name,email,phone_number,password) values(?,?,?,?,?)");
				ps.setString(1, user.getFirst_name());
				ps.setString(2, user.getLast_name());
				ps.setString(3, user.getEmail());
				ps.setString(4, user.getPhone_number());
				ps.setString(5, user.getPassword());

				status = ps.executeUpdate();
			}		
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}

	// For Login
	public String checkLogin(User user) {
		UserDAOImpl aux=new UserDAOImpl();
		try {
			Connection con = aux.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from user where email=? and password=?");
			ps.setString(1, user.getEmail());
			ps.setString(2, user.getPassword());

			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString("email");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	//insert users in user_login table 
	public int insertDataInUserLogin(User user) {
		int status = 0;
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("insert into user_login(user_id,token) values( ?, ?)");
			User u = getUserDataByEmail(user.getEmail());
			int id= u.getId();
			if(id!=-1) {
				ps.setInt(1, id);
				ps.setString(2, user.getEmail());
				
				status = ps.executeUpdate();
			}
			con.close();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}
	//get user data by email
	public User getUserDataByEmail(String email) {
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("select * from user where email=?");
			ps.setString(1, email);
			ResultSet rs = ps.executeQuery();
			User u= new User();
			if (rs.next()) {
				u.setId(rs.getInt("user_id"));
				u.setFirst_name(rs.getString("first_name"));
				u.setLast_name(rs.getString("last_name"));
				u.setEmail(rs.getString("email"));
				u.setPhone_number(rs.getString("phone_number"));
				u.setPassword(rs.getString("password"));
				return u;
			}
			con.close();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	// update data
	public int changePasswordByEmail(User user) {
		int status = 0;
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("update user set password=? where email=?");
			ps.setString(2, user.getEmail());			
			ps.setString(1, user.getPassword());

			status = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}

	public  List<User> getUsers() {
		List<User> users = new ArrayList<User>();
		Connection con = getConnection();
		try (Statement st = con.createStatement()) {
			st.execute("select * from user");
			ResultSet rs = st.getResultSet();
			while (rs.next()) {
				User u = new User();
				u.setId(rs.getInt("user_id"));
				u.setFirst_name(rs.getString("first_name"));
				u.setLast_name(rs.getString("last_name"));
				u.setEmail(rs.getString("email"));
				u.setPhone_number(rs.getString("phone_number"));
				u.setPassword(rs.getString("password"));
				users.add(u);

			}
			return users;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Reservation> getUserReservations(String email) {
		List<Reservation> reservations = new ArrayList<Reservation>();
		User user=getUserDataByEmail(email);
		
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("select * from reservations where user_id=?");
			ps.setInt(1, user.getId());
			
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Reservation r=new Reservation();
				r.setUser_id(rs.getInt("user_id"));
				r.setHotel_id(rs.getInt("hotel_id"));
				r.setCheck_in(rs.getString("check_in"));
				r.setCheck_out(rs.getString("check_out"));
				r.setRoom_number(rs.getInt("room_number"));
				r.setSpecial_requests(rs.getString("special_requests"));
				reservations.add(r);
			}
			return reservations;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	public List<ReservationFlight> getUserFlights(String email){
		List<ReservationFlight> reservations = new ArrayList<ReservationFlight>();
		User user=getUserDataByEmail(email);
		
		try {
			Connection con = getConnection();
			PreparedStatement ps = con.prepareStatement("select * from ReservationsFlights where user_id=?");
			ps.setInt(1, user.getId());
			
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				ReservationFlight r=new ReservationFlight();
				r.setUser_id(rs.getInt("user_id"));
				r.setFlight_id(rs.getInt("flight_id"));
				r.setLeave_date(rs.getString("leave_date"));
				r.setLeave_hour(rs.getString("leave_hour"));
				r.setArrive_date(rs.getString("arrive_date"));
				r.setArrive_hour(rs.getString("arrive_hour"));
				r.setPrice_reservation(rs.getDouble("price_seats"));
				r.setNumber_seats(rs.getInt("number_seats"));
				
				reservations.add(r);
			}
			return reservations;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public int updateUser(User user) {
		// TODO Auto-generated method stub
		return 0;
	}

}
