package ro.h23.manager.webservices.flights;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ro.h23.manager.core.flight.ReservationFlight;
import ro.h23.manager.db.FlightDAOImpl;



@Path("createReservationFlight")
public class CreateReservationFlight {
	@POST
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public String createReservation(ReservationFlight reservationFlight) {
		
		FlightDAOImpl flightReservationDao = new FlightDAOImpl();
		int status=flightReservationDao.createReservationFlight(reservationFlight);
		if(status != 0) {
			flightReservationDao.updateSeatsAvailable(reservationFlight.getFlight_id(), reservationFlight.getNumber_seats());
			return "<h3 style=\"color:lightgreen; text-align:center\">Your reservation has been completed successfully.</h3>";
			
		}else {
			return "<h3 style=\"color:#ff8080; text-align:center\">There was a problem with your reservation. Please try again</h3>";
		}
		
	}
	
}
