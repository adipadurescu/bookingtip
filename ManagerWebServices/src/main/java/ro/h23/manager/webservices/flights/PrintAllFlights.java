package ro.h23.manager.webservices.flights;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ro.h23.manager.core.flight.Flight;
import ro.h23.manager.db.FlightDAOImpl;

@Path("/printAllFlights")
public class PrintAllFlights {
	@POST
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public String printAllFlights(Flight f) {
		FlightDAOImpl flightDao = new FlightDAOImpl();
		List<Flight> flights = flightDao.getFlightsByUserPreferences(f.getSource_flight(), f.getDestination_flight(),
				f.getLeave_date(), f.getArrive_date());
		String ret = "";
		for (Flight flight : flights) {
			ret += "<div class=\"col-md-6 col-sm-6 animate-box\">" + "<div class=\"hotel-entry\">"
					+ "		<a href=\"ReservationServlet?flight_id=" + flight.getFlight_id() + "&price="
					+ flight.getPrice_seat() + "&seats=" + flight.getAvailable_seats() +"&leave=" + flight.getLeave_hour() + "&arrive="+flight.getArrive_hour()
					+ "\" class=\"hotel-img\" style=\"background-image: url(" + flight.getPlane().getLogo() + ");\">"
					+ "<p class=\"price\"><span>" + flight.getPrice_seat() + "&euro;"
					+ "</span><small> /seat</small></p></a>" + "<div class=\"desc\">"
					+ "<h3><a href=\"hotel-room.html\">" + flight.getPlane().getCompany() + "</a></h3>"
					+ "<span class=\"place\">Leave hour: " + flight.getLeave_hour() + "<br>Arrive hour: "
					+ flight.getArrive_hour() + "<br>Available seats: " + flight.getAvailable_seats() + "</span>"
					+ "<p>The plane leaves from " + flight.getSource_aeroport() + " and arrives at "
					+ flight.getDestination_aeropot() + "."
							+ "</p></div>" + "</div></div>";
		}
		System.out.println(ret);
		return ret;
	}

}
