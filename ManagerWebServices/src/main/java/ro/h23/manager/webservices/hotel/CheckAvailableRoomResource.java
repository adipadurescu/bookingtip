package ro.h23.manager.webservices.hotel;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ro.h23.manager.core.hotel.CheckAvailableRoom;
import ro.h23.manager.core.hotel.HotelRoom;
import ro.h23.manager.db.HotelDAOImpl;
import ro.h23.manager.db.HotelRoomDAOImpl;

@Path("/checkAvailableRooms")
public class CheckAvailableRoomResource {
	
	@POST
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public String checkAvailableRooms(CheckAvailableRoom checkAvailableRoom) {
		
		HotelRoomDAOImpl hotelRoomDao = new HotelRoomDAOImpl();
		HotelDAOImpl hotelDao = HotelDAOImpl.instance();
		HotelRoom hotelRoom =hotelRoomDao.getHotelRoomsByHotelIdAndTypeAndCheckIn(checkAvailableRoom);
		String ret="";
		if(hotelRoom != null) {
			ret+="<h3><label><p style=\"color:yellow\">Your reservation data</p></label></h3>"
				+ "<h4><label>Hotel Name: "+hotelDao.getHotelNameById(checkAvailableRoom.getHotelRoom().getHotel_id())+"</label></h4>"
				+"<h4><label>Room Type: "+checkAvailableRoom.getHotelRoom().getType()+"</label></h4>"
				+"<h4><label>Check in date: "+checkAvailableRoom.getCheckIn()+"</label></h4>"
				+"<h4><label>Check out date: "+checkAvailableRoom.getCheckOut()+"</label></h4>"
				+"<h4><label>Price for "+hotelRoomDao.calculateNumberOfNights(checkAvailableRoom)+" nights: "
				+hotelRoomDao.getTotalPriceByTypeAndHotelId(checkAvailableRoom)+"$ </label></h4>";
			return ret;
		}else {
			return "<h3><label><p style=\"color:yellow\">Your reservation data</p></label></h3>"
					+ "<h3 style=\"color:#ff8080; text-align:center\">Unavailable rooms for the specified period.</h3>";
		}
		
	}

}
