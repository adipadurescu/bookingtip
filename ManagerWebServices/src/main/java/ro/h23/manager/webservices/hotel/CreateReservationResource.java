package ro.h23.manager.webservices.hotel;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ro.h23.manager.core.hotel.Reservation;
import ro.h23.manager.db.HotelRoomDAOImpl;

@Path("/createReservation")
public class CreateReservationResource {
	
	@POST
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public String createReservation(Reservation reservation) {
		
		HotelRoomDAOImpl hotelRoomDao = new HotelRoomDAOImpl();
		int status=hotelRoomDao.createReservation(reservation);
		if(status != 0) {
			return "<h3 style=\"color:lightgreen; text-align:center\">Your reservation has been completed successfully.</h3>";
		}else {
			return "<h3 style=\"color:#ff8080; text-align:center\">There was a problem with your reservation. Please try again</h3>";
		}
		
	}
}
