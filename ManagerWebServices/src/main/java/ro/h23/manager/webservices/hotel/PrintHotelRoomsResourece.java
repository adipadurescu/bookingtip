package ro.h23.manager.webservices.hotel;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ro.h23.manager.core.hotel.HotelRoom;
import ro.h23.manager.core.hotel.HotelRoomDAO;
import ro.h23.manager.db.HotelDAOImpl;
import ro.h23.manager.db.HotelRoomDAOImpl;


@Path("/printHotelRooms")
public class PrintHotelRoomsResourece {
	@POST
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public String printHotelRooms(HotelRoom hotelRoom) {
		HotelRoomDAO hotelRoomDao = new HotelRoomDAOImpl();
		HotelDAOImpl hotelDao = HotelDAOImpl.instance();
		List<HotelRoom> hotelRooms = hotelRoomDao.getHotelRoomsByHotelId(hotelRoom.getHotel_id());
		String ret="<div class=\"col-md-12 col-md-offset-0 heading2 animate-box\">" + 
				"		<h2>"+hotelDao.getHotelNameById(hotelRoom.getHotel_id())+"</h2>" + 
				"	</div> "+
				"   <div class=\"row\">";
		for (HotelRoom h : hotelRooms) {
			ret+=   "		<div class=\"col-md-12 animate-box\">" + 
					"			<div class=\"room-wrap\">" + 
					"				<div class=\"row\">" + 
					"					<div class=\"col-md-6 col-sm-6\">" + 
					"						<div class=\"room-img\"" + 
					"							style=\"background-image: url("+h.getPicture()+");\"></div>" + 
					"					</div>" + 
					"					<div class=\"col-md-6 col-sm-6\">" + 
					"						<div class=\"desc\">" + 
					"							<h2>"+h.getType()+"</h2>" + 
					"							<p class=\"price\">" + 
					"								<span>$"+h.getPrice()+"</span> <small>/ night</small>" + 
					"							</p>" + 
					"							<p>"+h.getDescription()+"</p>" + 
					"							<p>\r\n" + 
					"								<form method=\"post\" action=\"ReservationInfoServlet\" ><input type=\"hidden\" name=\"hotel_id\" value=\""+h.getHotel_id()+"\"/> <button id=\"myBtn\" name=\"type\" value=\""+h.getType()+"\">  <a class=\"btn btn-primary\">Book Now!</a></button>" + 
					"							</p>" + 
					"						</div>" + 
					"					</div>" + 
					"				</div>" + 
					"			</div>" +
			        "       </div>";
		}
		ret += "</div>";
		return ret;

	}

}
