package ro.h23.manager.webservices.hotel;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ro.h23.manager.core.hotel.Hotel;
import ro.h23.manager.db.HotelDAOImpl;

@Path("printHotelsByStarRating")
public class PrintHotelsByStarRatingResource {
	@POST
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public String printHotelsByUserPreferences(Hotel h) {
		HotelDAOImpl hotelDao = HotelDAOImpl.instance();
		List<Hotel> hotels;
		if(h.getLocation()!=null) {
			hotels = hotelDao.getHotelsByStarRatingWithLocation(h.getLocation(),h.getStar_rating());
		}else {
			hotels = hotelDao.getHotelsByStarRating(h.getStar_rating());
		}
		
		String ret="";
		for (Hotel hotel : hotels) {
			ret += "<div class=\"col-md-6 col-sm-6 animate-box\">"
					+ "	<div class=\"hotel-entry\">"
					+ "		<a href=\"PrintHotelRoomsServlet?hotel_id="+hotel.getHotel_id()+"\" class=\"hotel-img\" style=\"background-image: url("+hotel.getPicture()+");\">"
					+ "		</a>"
					+ "		<div class=\"desc\">"
					+ "			<p class=\"star\"><span>";
			for (int i=0;i<hotel.getStar_rating();i++) {
				ret+="<i class=\"icon-star-full\"></i>";
			}
			ret +=              "</span></p>"
					+ "			<h3><a href=\"PrintHotelRoomsServlet?hotel_id="+hotel.getHotel_id()+"\">"+hotel.getName()+"</a></h3>"
					+ "			<span class=\"place\">"+hotel.getLocation()+"</span>"
					+ "			<p>"+hotel.getDescription()+"</p>"
					+ "		</div>" + "</div>"
					+ "	</div>";
		}

		return ret;

	}
}
