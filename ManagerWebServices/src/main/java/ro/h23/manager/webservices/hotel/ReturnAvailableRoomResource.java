package ro.h23.manager.webservices.hotel;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ro.h23.manager.core.hotel.CheckAvailableRoom;
import ro.h23.manager.core.hotel.HotelRoom;
import ro.h23.manager.db.HotelRoomDAOImpl;

@Path("/returnAvailableRoom")
public class ReturnAvailableRoomResource {

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public HotelRoom createReservation(CheckAvailableRoom checkAvailableRoom) {
		
		HotelRoomDAOImpl hotelRoomDao = new HotelRoomDAOImpl();
		HotelRoom hotelRoom =hotelRoomDao.getHotelRoomsByHotelIdAndTypeAndCheckIn(checkAvailableRoom);
		if(hotelRoom != null) {
			return hotelRoom;
		}else {
			return null;
		}
		
	}
}
