package ro.h23.manager.webservices.user;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ro.h23.manager.core.user.User;
import ro.h23.manager.db.UserDAOImpl;

@Path("changePasswordByEmail")
public class ChangePasswordResource {

	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
	public Boolean changePassword(User user) {
		UserDAOImpl userDao=new UserDAOImpl();
		int status=userDao.changePasswordByEmail(user);
		if(status!=0) {
			return true;
		}else {
			return false;
		}
	}
}
