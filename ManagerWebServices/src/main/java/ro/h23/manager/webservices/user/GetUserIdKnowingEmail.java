package ro.h23.manager.webservices.user;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ro.h23.manager.core.user.User;
import ro.h23.manager.db.UserDAOImpl;

@Path("/getUserIdKnowingEmail")
public class GetUserIdKnowingEmail {
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public User createReservation(User user) {
		UserDAOImpl userDao= new UserDAOImpl();
		User u=userDao.getUserDataByEmail(user.getEmail());
		return u;
		
	}
}
