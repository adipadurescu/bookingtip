package ro.h23.manager.webservices.user;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ro.h23.manager.core.user.LoginValidator;
import ro.h23.manager.core.user.User;
import ro.h23.manager.db.UserDAOImpl;

@Path("/login")
public class LoginResource {
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public LoginValidator loginUser(User user) throws Exception{
		UserDAOImpl checkingLog=new UserDAOImpl();
		LoginValidator lv = new LoginValidator();
		String email = checkingLog.checkLogin(user);
		if(email!=null) {
			int status=checkingLog.insertDataInUserLogin(user);
			if(status>0) {
				lv.setValidation("success");	
				lv.setEmail(email);
			}else {
				lv.setValidation("fail");
				lv.setEmail(email);
			}
		} else {
			lv.setValidation("fail");
			lv.setEmail(email);
		}
		return lv;
	}
	
}

