package ro.h23.manager.webservices.user;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ro.h23.manager.core.user.User;
import ro.h23.manager.core.user.UserDAO;
import ro.h23.manager.db.UserDAOImpl;

@Path("/printUserDataByEmail")
public class PrintUserDataByEmailResource {
	@POST
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public String printUserData(User user) {
		String ret="";
		UserDAO userDao = new UserDAOImpl();
		User u = userDao.getUserDataByEmail(user.getEmail());
		ret+="<h3><label><p style=\"color:yellow\">Your personal data</p></label></h3>"
			+ "<h4><label><p style=\"color:white\">First Name: "+u.getFirst_name()+"</p></label></h4>"
			+"<h4><label><p style=\"color:white\">Last Name: "+u.getLast_name()+"</p></label></h4>"
			+"<h4><label><p style=\"color:white\">Email: "+u.getEmail()+"</p></label></h4>"
			+"<h4><label><p style=\"color:white\">Phone number: "+u.getPhone_number()+"</p> </label></h4>";
		
		return ret;
		
	}
	
}
