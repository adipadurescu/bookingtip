package ro.h23.manager.webservices.user;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


import ro.h23.manager.core.flight.ReservationFlight;
import ro.h23.manager.core.user.User;
import ro.h23.manager.db.FlightDAOImpl;
import ro.h23.manager.db.UserDAOImpl;

@Path("/printUserFlights")
public class PrintUserFlightsResource {
	@POST
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public String printUserReservationData(User user) {
		String ret="";
		UserDAOImpl userDao = new UserDAOImpl();
		FlightDAOImpl flightDao=new FlightDAOImpl();
		List<ReservationFlight> reservationFlights= userDao.getUserFlights(user.getEmail());
		ret+="<h3><label><p style=\"color:yellow\">Your flights reservation</p></label></h3>";
		for (ReservationFlight r : reservationFlights) {
			ret+="<hr><h4><label><p style=\"color:white\">Company: "+flightDao.getInfoFlight(r.getFlight_id()).getPlane().getCompany()+"</p></label></h4>"
					+"<h4><label><p style=\"color:white\">Source: "+flightDao.getInfoFlight(r.getFlight_id()).getSource_aeroport()+ " from "+flightDao.getInfoFlight(r.getFlight_id()).getSource_flight() +"</p></label></h4>"
					+"<h4><label><p style=\"color:white\">Destination: "+flightDao.getInfoFlight(r.getFlight_id()).getDestination_aeropot()+ " from "+flightDao.getInfoFlight(r.getFlight_id()).getDestination_flight() +"</p></label></h4>"
					+"<h4><label><p style=\"color:white\">Source date: "+r.getLeave_date()+" at "+ r.getLeave_hour()+"</p></label></h4>"
					+"<h4><label><p style=\"color:white\">Destination date: "+r.getArrive_date()+" at "+ r.getArrive_hour()+"</p></label></h4>"
					+"<h4><label><p style=\"color:white\">Number of tickets: "+r.getNumber_seats()+"</p></label></h4>"
					+"<h4><label><p style=\"color:white\">Price: "+r.getPrice_reservation()+"&euro;</p> </label></h4>";
			
		}
		
		return ret;
		
	}
}
