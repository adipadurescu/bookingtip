package ro.h23.manager.webservices.user;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ro.h23.manager.core.hotel.Reservation;
import ro.h23.manager.core.user.User;
import ro.h23.manager.db.HotelDAOImpl;
import ro.h23.manager.db.UserDAOImpl;

@Path("/printUserReservation")
public class PrintUserReservationsResource {
	@POST
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_JSON)
	public String printUserReservationData(User user) {
		String ret="";
		UserDAOImpl userDao = new UserDAOImpl();
		HotelDAOImpl hotelDao=new HotelDAOImpl();
		List<Reservation> reservations= userDao.getUserReservations(user.getEmail());
		ret+="<h3><label><p style=\"color:yellow\">Your reservations</p></label></h3>";
		for (Reservation r : reservations) {
			ret+="<hr><h4><label><p style=\"color:white\">Hotel Name: "+hotelDao.getHotelNameById(r.getHotel_id())+"</p></label></h4>"
					+"<h4><label><p style=\"color:white\">Room number: "+r.getRoom_number()+"</p></label></h4>"
					+"<h4><label><p style=\"color:white\">Check in: "+r.getCheck_in()+"</p></label></h4>"
					+"<h4><label><p style=\"color:white\">Check out: "+r.getCheck_out()+"</p> </label></h4>";
			if(!r.getSpecial_requests().equals("")) {
				ret+="<h4><label><p style=\"color:white\">Special requests: "+r.getSpecial_requests()+"</p> </label></h4>";
			}
		}
		
		return ret;
		
	}
}
