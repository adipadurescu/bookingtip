package ro.h23.manager.webservices.user;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ro.h23.manager.core.user.User;
import ro.h23.manager.db.UserDAOImpl;


@Path("/register")
public class RegisterResource {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Boolean registerUser(User user) throws Exception{

		UserDAOImpl register_user=new UserDAOImpl();
		int status =register_user.registerUser(user);
		if (status > 0) {
			return true;
		} else {
			return false;
		}
	}
}
