package ro.h23.manager.webservices.user;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ro.h23.manager.core.user.User;
import ro.h23.manager.mail.Mailer;

@Path("/sendForgotPasswordMail")
public class SendForgotPasswordMailResource {
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Boolean sendRegMail(User user) {
		Boolean recv=Mailer.send("travelbooking18@gmail.com", "secretpassword!18", user.getEmail(), "Change your password",
				"<h2>Hello, in case you want to change your password, access the link below \n </h2>"
				+ "<a href=\"http://localhost:8081/ManagerWebClient/changePassword.jsp?email="+user.getEmail()+"\">http://localhost:8081/ManagerWebClient/changePassword.jsp?email="+user.getEmail()+"></a>"
				+ "<h3>Best regards,</h3>"
				+ "<h3>Travel Booking Team</h3>");
		
		return recv;
	}
}

