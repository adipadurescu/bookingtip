package ro.h23.manager.webservices.user;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ro.h23.manager.core.user.User;
import ro.h23.manager.mail.Mailer;

@Path("/sendRegistrationMail")
public class SendRegistrationMail {
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public Boolean sendRegMail(User user) {
		Boolean recv=Mailer.send("travelbooking18@gmail.com", "secretpassword!18", user.getEmail(), "Welcome to Travel Booking!",
				"<h2>Hi "+user.getLast_name().substring(0, 1).toUpperCase()+user.getLast_name().substring(1)+" "+user.getFirst_name().substring(0, 1).toUpperCase()+user.getFirst_name().substring(1)+"!</h2><br/>"
				+ "<h3>The registration for Travel Booking is confirmed!<h3>"
				+ "<h3>On our website you can buy tickets to your favourite destinations and find accomodation in a simple way. </h3>"
				+ "<h3>Thank you for choosing Travel Booking!</h3><br/>"
				+ "<h3>Best regards,</h3>"
				+ "<h3>Travel Booking Team</h3>");
		
		return recv;
	}
}
